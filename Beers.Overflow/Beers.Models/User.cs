﻿using Beers.Models.Abstract;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Beers.Models
{
    public class User : IdentityUser<int>
    {

        public ICollection<BeerReview> BeerReviews { get; set; } = new List<BeerReview>();

        public ICollection<UserBeerRelations> UserBeerRelations { get; set; } = new List<UserBeerRelations>();

        public ICollection<BeerRating> BeerRatings { get; set; } = new List<BeerRating>();

        public ICollection<ReviewVote> ReviewVotes { get; set; } = new List<ReviewVote>();

        public ICollection<Beer> CreatedBeers { get; set; } = new List<Beer>();

    }
}
