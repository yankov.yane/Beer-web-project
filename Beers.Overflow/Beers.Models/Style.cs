﻿using Beers.Models.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Beers.Models
{
    public class Style : Entity
    {
        [Key] 
        public int Id { get; set; }

        [Required]
        [MinLength(2), MaxLength(150)]
        public string StyleType { get; set; }

        public virtual ICollection<Beer> Beers { get; set; } = new List<Beer>();
    }
}
