﻿using Beers.Models.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Beers.Models
{
    public class BeerReview : UserBeerLink
    {
        public BeerReview( int userId, int beerId, string content, string userName)
        {
            Content = content;
            BeerId = beerId;
            UserId = userId;
            UserName = userName;
        }
        public BeerReview()
        {

        }

        [Key] 
        public int Id { get; set; }

        [Required]
        [MinLength(5), MaxLength(1000)]
        public string Content { get; set; }

        public ICollection<ReviewVote> ReviewVotes { get; set; } = new List<ReviewVote>();

        public bool IsReported { get; set; }

        public string UserName { get; set; }
    }
}
  