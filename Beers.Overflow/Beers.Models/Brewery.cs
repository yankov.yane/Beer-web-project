﻿using Beers.Models.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Beers.Models
{
    public class Brewery : Entity
    {
        [Key] 
        public int Id { get; set; }

        [Required]
        [MinLength(2), MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MinLength(5), MaxLength(150)]
        public string Address { get; set; }

        public int CountryId { get; set; }
        public Country Country { get; set; }

        public ICollection<Beer> Beers { get; set; } = new List<Beer>();

    }
}
