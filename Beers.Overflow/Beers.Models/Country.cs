﻿using Beers.Models.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Beers.Models
{
    public class Country : Entity
    {
        [Key] 
        public int Id { get; set; }

        [Required]
        [MinLength(2), MaxLength(65)]
        public string Name { get; set; }

        public ICollection<Brewery> Breweries { get; set; } = new List<Brewery>();
    }
}
