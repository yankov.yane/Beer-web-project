﻿using Beers.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Beers.Models
{
    public class ReviewVote : Entity
    {
        public ReviewVote(int reviewId, int userId, bool like)
        {
            ReviewId = reviewId;
            UserId = userId;
            Like = like;
        }
        public ReviewVote()
        {

        }

        [Key] 
        public int Id { get; set; }

        [Required]
        public int ReviewId { get; set; }
        public BeerReview Review { get; set; }

        [Required]
        public int UserId { get; set; }
        public User User { get; set; }

        public bool? Like { get; set; }

       
    }
}
