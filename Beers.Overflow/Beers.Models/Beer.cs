﻿using Beers.Models.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Beers.Models
{
    public class Beer : Entity
    {

        [Key] 
        public int Id { get; set; }
        
        [Required]
        [MinLength(2), MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [MinLength(10), MaxLength(500)]
        public string Description { get; set; }

        [Required]
        [MinLength(2), MaxLength(50)]
        public string Brand { get; set; }

        [Required]
        public int BreweryId { get; set; }
        public Brewery Brewery { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,2)")]
        public decimal ABV { get; set; }

        [Required]
        public int StyleId { get; set; }
        public Style Style { get; set; }

        public int? CreatedByUserId { get; set; }
        public User CreatedByUser { get; set; }

        public ICollection<BeerReview> BeerReviews { get; set; } = new List<BeerReview>();

        public ICollection<BeerRating> BeerRatings { get; set; } = new List<BeerRating>();

        public ICollection<UserBeerRelations> UserBeerRelations { get; set; } = new List<UserBeerRelations>();

    }
}
