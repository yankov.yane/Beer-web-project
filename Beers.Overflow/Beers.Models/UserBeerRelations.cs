﻿using Beers.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Beers.Models
{//profanity filters
    public class UserBeerRelations : UserBeerLink
    {
        [Key] 
        public int Id { get; set; }

        public bool Wish { get; set; }
        public bool Drank { get; set; }
    }
}
