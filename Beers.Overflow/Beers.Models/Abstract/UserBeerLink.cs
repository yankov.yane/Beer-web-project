﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Beers.Models.Abstract
{
    public abstract class UserBeerLink : Entity
    {
        [Required]
        public int BeerId { get; set; }
        public Beer Beer { get; set; }

        [Required]
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
