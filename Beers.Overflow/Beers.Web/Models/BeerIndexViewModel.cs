﻿using Beers.Services.Contracts;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Web.Models
{
    public class BeerIndexViewModel
    {
        private IBeerService _serviceBeers;
        private IStyleService _serviceStyles;
        private IBreweryService _serviceBrewery;
        public BeerIndexViewModel(IBeerService service, IStyleService serviceStyles, IBreweryService serviceBrewery)
        {
            this._serviceBeers = service;
            this._serviceStyles = serviceStyles;
            this._serviceBrewery = serviceBrewery;

            StylesDropDown = GetStyles();
            BreweriesDropDown = GetBreweries();
            BeersList = GetBeers().Result.ToList();
        }

        public BeerIndexViewModel()
        {

        }



        [Required]
        public string StyleIdToFilter { get; set; }
        [Required]
        public string BreweryIdToFilter { get; set; }

        public bool OrderByABV { get; set; }

        public string Query { get; set; }


        public List<SelectListItem> StylesDropDown { get; set; }
        public List<SelectListItem> BreweriesDropDown { get; set; }


        public List<BeerViewModel> BeersList { get; set; }


        public async Task<IEnumerable<BeerViewModel>> GetBeers()
        {
            var beers = await _serviceBeers.GetAllBeers();
            return beers.Select(beer => new BeerViewModel(beer));
        }

        private List<SelectListItem> GetStyles()
        {
            List<SelectListItem> styles = new List<SelectListItem>();
            styles.Add(new SelectListItem() { Text = "No Select", Value = "No Select" });

            foreach (var item in _serviceStyles.GetAllStyles().Result)
            {
                string text = item.StyleType;
                string value = item.Id.ToString();

                styles.Add(new SelectListItem() { Text = text, Value = value });
            }
            return styles;
        }

        private List<SelectListItem> GetBreweries()
        {
            List<SelectListItem> breweries = new List<SelectListItem>();
            breweries.Add(new SelectListItem() { Text = "No Select", Value = "No Select" });

            foreach (var item in _serviceBrewery.GetAllBreweries().Result)
            {
                string text = item.Name;
                string value = item.Id.ToString();

                breweries.Add(new SelectListItem() { Text = text, Value = value });
            }
            return breweries;
        }
    }


}
