﻿using Beers.Services.DTOs;
using System;

namespace Beers.Web.Models
{
    public class StyleViewModel
    {
        public StyleViewModel(StylesDTO style)
        {
            Id = style.Id;
            StyleType = style.StyleType;

            CreatedOn = style.CreatedOn;
            ModifiedOn = style.ModifiedOn;
            DeletedOn = style.DeletedOn;
            IsDeleted = style.IsDeleted;
        }

        public StyleViewModel()
        {

        }

        public int Id { get; set; }
        public string StyleType { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}