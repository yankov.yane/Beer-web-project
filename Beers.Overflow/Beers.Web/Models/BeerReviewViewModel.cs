﻿using Beers.Services.DTOs;
using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Web.Models
{
    public class BeerReviewViewModel
    {
        public BeerReviewViewModel(BeerReviewDTO review)
        {
            Id = review.Id;
            UserId = review.UserId;
            BeerId = review.BeerId;
            Content = review.Content;
            IsReported = review.IsReported;

            CreatedOn = review.CreatedOn;
            ModifiedOn = review.ModifiedOn;
            DeletedOn = review.DeletedOn;
            IsDeleted = review.IsDeleted;
        }

        public BeerReviewViewModel()
        {

        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public int BeerId { get; set; }
        public string Content { get; set; }
        public bool IsReported { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
