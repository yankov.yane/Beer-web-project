﻿using Beers.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Beers.Web.Models
{
    public class AdminPanelViewModel
    {
        public AdminPanelViewModel()
        {

        }

        public List<User> Users { get; set; } = new List<User>();
    }
}
