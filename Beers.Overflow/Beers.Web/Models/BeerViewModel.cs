﻿using Beers.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Web.Models
{
    public class BeerViewModel
    {
        public BeerViewModel(BeerDTO beerDTO)
        {
            this.Id = beerDTO.Id;
            this.Name = beerDTO.Name;
            this.Description = beerDTO.Description;
            this.Brand = beerDTO.Brand;
            this.ABV = beerDTO.ABV;
            this.StyleId = beerDTO.StyleId;
            this.BreweryId = beerDTO.BreweryId;

            this.CreatedOn = beerDTO.CreatedOn;
            this.ModifiedOn = beerDTO.ModifiedOn;
            this.IsDeleted = beerDTO.IsDeleted;
            this.DeletedOn = beerDTO.DeletedOn;
        }

        public BeerViewModel()
        {

        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Brand { get; set; }
        public decimal ABV { get; set; }
        public int StyleId { get; set; }
        public int BreweryId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
