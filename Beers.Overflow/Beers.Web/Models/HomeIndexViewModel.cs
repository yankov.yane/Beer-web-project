﻿using Beers.Web.Models;
using System.Collections.Generic;

namespace Beers.Web.Models
{
    public class HomeIndexViewModel
    {
        public List<BeerViewModel> TopRatedBeers { get; set; }
        public List<BreweryViewModel> AllBreweries { get; set; }

    }
}