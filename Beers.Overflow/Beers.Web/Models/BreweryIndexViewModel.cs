﻿using Beers.Services.Contracts;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Web.Models
{
    public class BreweryIndexViewModel
    {
        private IBreweryService _breweryService;
        private ICountryService _countryService;

        public BreweryIndexViewModel(IBreweryService breweryService, ICountryService countryService)
        {
            this._breweryService = breweryService;
            this._countryService = countryService;

            CountriesDropDown = GetCountriesDropDown();
        }

        public BreweryIndexViewModel()
        {

        }


        [Required]
        public string CountryIdToFilter { get; set; }

        public string Query { get; set; }


        public List<BreweryViewModel> BreweriesList { get; set; }
        public List<SelectListItem> CountriesDropDown { get; set; }




        private List<SelectListItem> GetCountriesDropDown()
        {
            List<SelectListItem> countries = new List<SelectListItem>();
            countries.Add(new SelectListItem() { Text = "No Select", Value = "No Select" });

            //var allCountries = _countryService.GetAllCountries().Result; ////Logika za da se pokazvat Countries kudeto ima assigned Breweries
            //var allBreweries = await GetBreweries();



            foreach (var item in _countryService.GetAllCountries().Result)
            {
                string text = item.Name;
                string value = item.Id.ToString();

                countries.Add(new SelectListItem() { Text = text, Value = value });
            }
            return countries;
        }

        public async Task<IEnumerable<BreweryViewModel>> GetBreweries()
        {
            var beers = await _breweryService.GetAllBreweries();
            return beers.Select(beer => new BreweryViewModel(beer));
        }
    }
}
