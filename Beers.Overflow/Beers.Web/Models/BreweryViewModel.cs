﻿using Beers.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Web.Models
{
    public class BreweryViewModel
    {
        public BreweryViewModel(BreweryDTO breweryDTO)
        {
            this.Id = breweryDTO.Id;
            this.Name = breweryDTO.Name;
            this.Address = breweryDTO.Address;
            this.CountryId = breweryDTO.CountryId;
            this.CreatedOn = breweryDTO.CreatedOn;
            this.ModifiedOn = breweryDTO.ModifiedOn;
            this.IsDeleted = breweryDTO.IsDeleted;
            this.DeletedOn = breweryDTO.DeletedOn;
        }

        public BreweryViewModel()
        {

        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int CountryId { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
