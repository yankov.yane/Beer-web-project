﻿using Beers.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Web.Models
{
    public class WishListViewModel
    {
        public List<BeerViewModel> WishList { get; set; }
    }
}
