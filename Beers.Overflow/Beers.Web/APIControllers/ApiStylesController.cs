﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Beers.Web.APIControllers
{
    [Route("api/[controller]")] // www.domainname.com/api/Styles
    [ApiController]
    public class ApiStylesController : ControllerBase
    {
        private readonly IStyleService serviceService;

        public ApiStylesController(IStyleService serviceService)
        {
            this.serviceService = serviceService;
        }

        //GET api/Styles
        [HttpGet("")]
        public async Task<IActionResult> Get([FromQuery] string order)
        {
            return Ok(await this.serviceService.GetAllStyles());
        }

        //GET api/Styles/:id
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var style = await this.serviceService.GetStyle(id);

            if (style == null)
            {
                return NotFound();
            }

            return Ok(style);
        }

        //GET api/Styles/:id
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAllStyles()
        {
            var styles = await this.serviceService.GetAllStyles();

            if (styles == null)
            {
                return NotFound();
            }

            var styleViews = styles.Select(x => new StyleViewModel(x));

            return Ok(styleViews);
        }

        //PUT api/Styles/:id
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] StyleViewModel model) //Update
        {
            if (id < 1 || model == null)
            {
                return BadRequest();
            }

            var styleDTO = new StylesDTO
            {
                Id = model.Id,
                StyleType = model.StyleType
            };

            var updatedBeerDTO = await this.serviceService.Update(id, styleDTO);

            return Ok(updatedBeerDTO);
        }

        //POST api/Styles
        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] StyleViewModel model) //Create
        {
            if (model == null)
            {
                return BadRequest();
            }

            var styleDTO = new StylesDTO
            {
                Id = model.Id,
                StyleType = model.StyleType
            };

            var style = await this.serviceService.Create(styleDTO);

            return Created("post", style);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await this.serviceService.Delete(id);

            if (result == true)
            {
                return NoContent();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
