﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Beers.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApiBreweriesController : ControllerBase
    {
        private readonly IBreweryService BreweryService;

        public ApiBreweriesController(IBreweryService breweryService)
        {
            this.BreweryService = breweryService;
        }

        //GET api/breweries
        [HttpGet("")]
        public async Task<IActionResult> Get([FromQuery] string order)
        {
            return Ok( await this.BreweryService.GetAllBreweries());
        }

        //GET api/beers/:id
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var brewery = await this.BreweryService.GetBrewery(id);

            if (brewery == null)
            {
                return NotFound();
            }

            return Ok(brewery);
        }

        //POST api/beers
        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] BreweryViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            var breweryDTO = new BreweryDTO
            {
                Id = model.Id,
                Name = model.Name,
                Address = model.Address
            };

            var brewery = await this.BreweryService.Create(breweryDTO);

            return Created("post", brewery);
        }

        //PUT api/beers/:id
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] BreweryViewModel model)
        {
            if (id < 1 || model == null)
            {
                return BadRequest();
            }

            var breweryDTO = new BreweryDTO
            {
                Id = model.Id,
                Name = model.Name,
                Address = model.Address
            };

            var updatedBreweryDTO = await this.BreweryService.Update(id, breweryDTO);

            return Ok(updatedBreweryDTO);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await this.BreweryService.Delete(id);

            if (result == true)
            {
                return NoContent();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
