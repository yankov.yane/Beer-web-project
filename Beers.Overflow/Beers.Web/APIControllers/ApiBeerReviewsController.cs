﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Beers.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApiBeerReviewsController : ControllerBase
    {
        private readonly IBeerReviewService beerReviewService;

        public ApiBeerReviewsController(IBeerReviewService beerReviewService)
        {
            this.beerReviewService = beerReviewService;
        }

        //GET api/beerReviews/:id
        [HttpGet("{id}")]
        public async Task<IActionResult> GetByUser(int id)
        {
            var beerReviews = await this.beerReviewService.GetReviewsByUser(id);

            if (beerReviews == null)
            {
                return NotFound();
            }

            return Ok(beerReviews);
        }

        //GET api/beerReviews/:id
        [HttpGet("{id}")]
        public async Task<IActionResult> GetByBeer(int id)
        {
            var beerReviews = await this.beerReviewService.GetReviewsByBeer(id);

            if (beerReviews == null)
            {
                return NotFound();
            }

            return Ok(beerReviews);
        }

        //POST api/beerReviews
        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] BeerReviewViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            var beerReviewDTO = new BeerReviewDTO
            {
                Id = model.Id,
                UserId = model.UserId,
                BeerId = model.BeerId,
                Content = model.Content
            };

            var beerReview = await this.beerReviewService.Create(beerReviewDTO);

            return Created("post", beerReview);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await this.beerReviewService.Delete(id);

            if (result == true)
            {
                return NoContent();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> GetLikes(int id)
        {
            var result = await this.beerReviewService.GetLikeCount(id);

            if (result == 0)
            {
                return NoContent();
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> GetDislikes(int id)
        {
            var result = await this.beerReviewService.GetDislikeCount(id);

            if (result == 0)
            {
                return NoContent();
            }
            else
            {
                return Ok(result);
            }
        }
    }
}
