﻿using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApiBeersController : ControllerBase
    {
        private readonly IBeerService beerService;

        public ApiBeersController(IBeerService beerService)
        {
            this.beerService = beerService;
        }

        //GET api/beers
        [HttpGet("")]
        public async Task<IActionResult> Get([FromQuery] string order)
        {
            return Ok(await this.beerService.GetAllBeers());
        }

        //GET api/beers/:id
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var beerDTO = await this.beerService.GetBeer(id);

            if (beerDTO == null)
            {
                return NotFound();
            }

            var beerView = new BeerViewModel(beerDTO);

            return Ok(beerView);
        }

        //POST api/beers
        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] BeerViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            var beerDTO = new BeerDTO
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                ABV = model.ABV
            };

            var beer = await this.beerService.Create(beerDTO);

            return Created("post", beer);
        }

        //PUT api/beers/:id
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] BeerViewModel model)
        {
            if (id < 1 || model == null)
            {
                return BadRequest();
            }

            var beerDTO = new BeerDTO
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                ABV = model.ABV
            };

            var updatedBeerDTO = await this.beerService.Update(id, beerDTO);

            return Ok(updatedBeerDTO);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await this.beerService.Delete(id);

            if (result == true)
            {
                return NoContent();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
