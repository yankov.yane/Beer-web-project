﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace Beers.Web.APIControllers
{
    [Route("api/[controller]")] // www.domainname.com/api/Countries
    [ApiController]
    public class ApiCountriesController : ControllerBase
    {

        private readonly ICountryService countryService;

        public ApiCountriesController(ICountryService countryService)
        {
            this.countryService = countryService;
        }

        //GET api/countries
        [HttpGet("")] 
        public async Task<IActionResult> Get([FromQuery] string order)
        {
            return Ok(await this.countryService.GetAllCountries());
        }

        //GET api/country/:id
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var country = await this.countryService.GetCountry(id);

            if (country == null)
            {
                return NotFound();
            }

            return Ok(country);
        }


        //PUT api/country/:id
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] CountryViewModel model) //Update
        {
            if (id < 1 || model == null)
            {
                return BadRequest();
            }

            var countryDTO = new CountryDTO
            {
                Id = model.Id,
                Name = model.Name
            };

            var updatedBeerDTO = await this.countryService.Update(id, countryDTO);

            return Ok(updatedBeerDTO);
        }

        //POST api/beers
        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] CountryViewModel model) //Create
        {
            if (model == null)
            {
                return BadRequest();
            }

            var countryDTO = new CountryDTO
            {
                Id = model.Id,
                Name = model.Name
            };

            var country = await this.countryService.Create(countryDTO);

            return Created("post", country);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await this.countryService.Delete(id);

            if (result == true)
            {
                return NoContent();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
