﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Beers.Data.Context;
using Beers.Models;
using Beers.Services.Contracts;
using Beers.Web.Models;
using Beers.Services.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace Beers.Web.Controllers
{
    public class BreweriesController : Controller
    {
        private readonly IBreweryService _breweryService;
        private readonly ICountryService _countryService;

        public BreweriesController(IBreweryService service, ICountryService countryService)
        {
            _breweryService = service;
            _countryService = countryService;
        }

        // GET: Breweries
        public async Task<IActionResult> Index(string countryId, string query)
        {
            var breweriesIndex = new BreweryIndexViewModel(_breweryService, _countryService);
            var brewereis = await breweriesIndex.GetBreweries();

            if (countryId == "No Select" || countryId == null)
            {
                breweriesIndex.BreweriesList = brewereis.ToList();
            }
            else
            {
                breweriesIndex.BreweriesList = brewereis.Where(x => x.CountryId == int.Parse(countryId)).ToList();
            }


            if (query != "" && query != null)
            {
                breweriesIndex.BreweriesList = breweriesIndex.BreweriesList.Where(x => x.Name.Contains(query)).ToList();
            }

            return View(breweriesIndex);
        }

        public IActionResult FilterBreweries(BreweryIndexViewModel model)
        {
            return RedirectToAction("Index", "Breweries",
                    new
                    {
                        countryId = model.CountryIdToFilter,
                        query = model.Query
                    }); ;
        }

        // GET: Breweries/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var breweryDTO = await _breweryService.GetBrewery((int)id);

            if (breweryDTO == null)
            {
                return NotFound();
            }

            ViewData["CountryId"] = new SelectList(await _countryService.GetAllCountries(), "Id", "Name", breweryDTO.CountryId);

            var breweryView = new BreweryViewModel(breweryDTO);

            return View(breweryView);
        }

        // GET: Breweries/Create
        [Authorize]
        public async Task<IActionResult> Create()
        {
            ViewData["CountryId"] = new SelectList(await _countryService.GetAllCountries(), "Id", "Name");

            return View();
        }

        // POST: Breweries/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("Id,Name,Address,CountryId")] BreweryDTO breweryDTO)
        {
            if (ModelState.IsValid)
            {
                await _breweryService.Create(breweryDTO);

                return RedirectToAction(nameof(Index));
            }

            ViewData["CountryId"] = new SelectList(await _countryService.GetAllCountries(), "Id", "Name", breweryDTO.CountryId);

            var breweryView = new BreweryViewModel(breweryDTO);

            return View(breweryView);
        }

        // GET: Breweries/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var breweryDTO = await _breweryService.GetBrewery((int)id);

            if (breweryDTO == null)
            {
                return NotFound();
            }

            ViewData["CountryId"] = new SelectList(await _countryService.GetAllCountries(), "Id", "Name", breweryDTO.CountryId);

            var breweryView = new BreweryViewModel(breweryDTO);

            return View(breweryView);
        }

        // POST: Breweries/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Address,CountryId")] BreweryDTO breweryDTO)
        {
            if (id != breweryDTO.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _breweryService.Update(id, breweryDTO);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BreweryExists(breweryDTO.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            ViewData["CountryId"] = new SelectList(await _countryService.GetAllCountries(), "Id", "Name", breweryDTO.CountryId);

            var breweryView = new BreweryViewModel(breweryDTO);

            return View(breweryView);
        }

        // GET: Breweries/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var brewery = await _breweryService.GetBrewery((int)id);

            if (brewery == null)
            {
                return NotFound();
            }

            return View(brewery);
        }

        // POST: Breweries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _breweryService.Delete(id);

            return RedirectToAction(nameof(Index));
        }

        private bool BreweryExists(int id)
        {
            var brewery = _breweryService.GetBrewery(id);

            if (brewery != null)
            {
                return true;
            }
            return false;
        }
    }
}
