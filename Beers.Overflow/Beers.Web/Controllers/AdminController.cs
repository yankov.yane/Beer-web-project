﻿using Beers.Services.Contracts;
using Beers.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly IUserService _userService;

        public AdminController(IUserService userService)
        {
            this._userService = userService;
        }

        [HttpGet]
        public async Task<IActionResult> UsersIndex()
        {
            var viewModel = new AdminPanelViewModel();
            viewModel.Users = (await this._userService.GetAllRegularUsers()).ToList();
            return View(viewModel);
        }
        //TODO: Ban user - Yane

        [HttpPost]
        public async Task<IActionResult> BanUsers(int userId)
        {
            bool isBanned = await _userService.BanUserById(userId);
            return RedirectToAction("UsersIndex", "Admin");

        }

        [HttpPost]
        public async Task<IActionResult> UnbanUsers(int userId)
        {
            bool isBanned = await _userService.UnbanUserById(userId);
            return RedirectToAction("UsersIndex", "Admin");

        }
    }
}
