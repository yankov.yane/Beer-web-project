﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Beers.Data.Context;
using Beers.Models;
using Beers.Services.Contracts;
using Beers.Web.Models;
using Beers.Services.DTOs;
using Beers.Services;
using System.ComponentModel.Design;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace Beers.Web.Controllers
{
    [Authorize]
    public class BeerReviewsController : Controller
    {
        private readonly IBeerReviewService _service;
        private readonly IBeerService _beerService;
        private readonly IUserService _userService;

        public BeerReviewsController(IBeerReviewService service, IBeerService beerService, IUserService userService)
        {
            _service = service;
            _beerService = beerService;
            _userService = userService;
        }

        // GET: BeerReviews
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            var allBeerReviewsDTO = await _service.GetAllReviews();

            var allBeerReviwesView = allBeerReviewsDTO.Select(x => new BeerReviewViewModel(x));

            return View(allBeerReviwesView);
        }

        // GET: BeerReviews/Details/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beerReview = await _service.GetById((int)id);

            if (beerReview == null)
            {
                return NotFound();
            }

            return View(beerReview);
        }

        //GET: BeerReviews/Create

        public async Task<IActionResult> CreateAsync(int? id)
        {
            var newReview = new BeerReviewViewModel()
            {
                BeerId = (int)id,
                UserId = await _userService.GetUserIdAsync(HttpContext)
            };

            return View(newReview);
        }

        // POST: BeerReviews/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateReview(BeerReviewDTO beerReview)
        {
            if (ModelState.IsValid)
            {
                var userId = await _userService.GetUserIdAsync(HttpContext);

                if ((await this._service.CheckIfReviewed(beerReview.BeerId, userId)) == true)
                {
                    return RedirectToAction("Details", "Beers", new { id = beerReview.BeerId, error = TempData["Error"] = "You cant review twice BOI!"});
                }

                await _service.Create(beerReview);
                return RedirectToAction("Details", "Beers", new { id = beerReview.BeerId});
            }

            return View(beerReview);
        }

        // GET: BeerReviews/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beerReview = await _service.GetById((int)id);

            if (beerReview == null)
            {
                return NotFound();
            }

            return View(beerReview);
        }

        // POST: BeerReviews/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Content,BeerId,UserId")] BeerReviewDTO beerReviewDTO)
        {
            if (id != beerReviewDTO.BeerId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _service.Update(id, beerReviewDTO);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BeerReviewExists(beerReviewDTO.BeerId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            var beerReviewViewModel = new BeerReviewViewModel(beerReviewDTO);

            return View(beerReviewViewModel);
        }

        // GET: BeerReviews/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beerReview = await _service.GetById((int)id);

            if (beerReview == null)
            {
                return NotFound();
            }

            return View(beerReview);
        }

        // POST: BeerReviews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _service.Delete(id);

            return RedirectToAction(nameof(Index));
        }

        private bool BeerReviewExists(int id)
        {
            if (_service.GetById(id) == null)
            {
                return false;
            }
            return true;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> LikeReview(int reviewId, int beerId)
        {
            var userId = await this._userService.GetUserIdAsync(HttpContext);

            if (await this._service.Like(reviewId, userId) == true)
            {
                return RedirectToAction("Details", "Beers", new { id = beerId, msg = TempData["Msg"] = "Review Liked !" });
            }

            return RedirectToAction("Details", "Beers", new { id = beerId, msg = TempData["Msg"] = "You have already liked that review!" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DislikeReview(int reviewId, int beerId)
        {
            var userId = await this._userService.GetUserIdAsync(HttpContext);

            if (await this._service.Dislike(reviewId, userId) == true)
            {
                return RedirectToAction("Details", "Beers", new { id = beerId, msg = TempData["Msg"] = "Review Disliked !" });
            }

            return RedirectToAction("Details", "Beers", new { id = beerId, msg = TempData["Msg"] = "You have already Disliked that review!" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> ReportReview(int reviewId, int beerId)
        {

            if (await this._service.Report(reviewId) == true)
            {
                return RedirectToAction("Details", "Beers", new { id = beerId, msg = TempData["Msg"] = "Review Reported !" });
            }

            return RedirectToAction("Details", "Beers", new { id = beerId, error = TempData["Error"] = "This review is already reported!" });
        }
    }
}
