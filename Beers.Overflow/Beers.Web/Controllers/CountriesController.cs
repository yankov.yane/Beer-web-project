﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Beers.Data.Context;
using Beers.Models;
using Beers.Services.Contracts;
using Beers.Web.Models;
using Beers.Services.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace Beers.Web.Controllers
{
    public class CountriesController : Controller
    {
        private readonly ICountryService service;

        public CountriesController(ICountryService service)
        {
            this.service = service;
        }

        // GET: Countries
        public async Task<IActionResult> Index()
        {
            var countries = await service.GetAllCountries();

            var countryViews = countries.Select(x => new CountryViewModel
            {
                Id = x.Id,
                Name = x.Name
            });

            return View(countryViews);
        }

        // GET: Countries/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var country = await service.GetCountry(id);

            if (country == null)
            {
                return NotFound();
            }

            return View(country);
        }

        // GET: Countries/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Countries/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("Name")] CountryDTO countryDTO)
        {
            if (ModelState.IsValid)
            {
                await service.Create(countryDTO);
                return RedirectToAction(nameof(Index));
            }

            var countryViews = new CountryViewModel
            {
                Name = countryDTO.Name
            };

            return View(countryViews);
        }

        // GET: Countries/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var country = await service.GetCountry((int) id);

            if (country == null)
            {
                return NotFound();
            }

            return View(country);
        }

        // POST: Countries/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("Name")] CountryDTO countryDTO)
        {
            if (id != countryDTO.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await service.Update(id , countryDTO);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CountryExists(countryDTO.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            var countryViews = new CountryViewModel
            {
                Name = countryDTO.Name
            };

            return View(countryViews);
        }

        // GET: Countries/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var country = await service.GetCountry((int)id);
            if (country == null)
            {
                return NotFound();
            }

            return View(country);
        }

        // POST: Countries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await service.Delete(id);

            return RedirectToAction(nameof(Index));
        }

        private bool CountryExists(int id)
        {
            var beer = service.GetCountry(id);

            if (beer != null)
            {
                return true;
            }
            return false;
        }
    }
}
