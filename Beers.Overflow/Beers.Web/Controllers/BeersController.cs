﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Beers.Services.Contracts;
using Beers.Web.Models;
using Beers.Services.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace Beers.Web.Controllers
{
    public class BeersController : Controller
    {
        private readonly IBeerService _service;
        private readonly IBreweryService _breweryService;
        private readonly IStyleService _styleService;
        private readonly IUserService _userService;
        private readonly IBeerReviewService _beerReviewService;

        public BeersController(IBeerService service,
                               IBreweryService breweryService,
                               IStyleService styleService,
                               IUserService userService,
                               IBeerReviewService beerReviewService)
        {
            this._service = service;
            this._breweryService = breweryService;
            this._styleService = styleService;
            this._userService = userService;
            this._beerReviewService = beerReviewService;
        }

        // GET: Beers
        public async Task<IActionResult> Index(string styleId, string breweryId, bool orderByABV, string query)
        {
            var beers = new BeerIndexViewModel(_service, _styleService, _breweryService);
            var resultedBeers = await beers.GetBeers();
            beers.Query = query;

            if (styleId == "No Select" || styleId == null)
            {
                beers.BeersList = resultedBeers.ToList();
            }
            else
            {
                beers.StyleIdToFilter = styleId;
                beers.BeersList = resultedBeers.Where(x => x.StyleId == int.Parse(styleId)).ToList();
            }


            if (breweryId != null && breweryId != "No Select")
            {
                beers.BreweryIdToFilter = breweryId;
                beers.BeersList = beers.BeersList.Where(x => x.BreweryId == int.Parse(breweryId)).ToList();
            }

            if (orderByABV == true)
            {
                beers.BeersList = beers.BeersList.OrderBy(x => x.ABV).ToList();
            }

            if (query != "" && query != null)
            {
                beers.BeersList = beers.BeersList.Where(x => x.Name.Contains(query)).ToList();
            }

            return View(beers);
        }

        [HttpPost]
        public IActionResult FilterStyle(BeerIndexViewModel beerIndexData)
        {

            return RedirectToAction("Index", "Beers",
                new
                {
                    styleId = beerIndexData.StyleIdToFilter,
                    breweryId = beerIndexData.BreweryIdToFilter,
                    orderByABV = beerIndexData.OrderByABV,
                    query = beerIndexData.Query,
                });
        }

        // GET: Beers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beerDTO = await _service.GetBeer((int)id);

            if (beerDTO == null)
            {
                return NotFound();
            }

            if (HttpContext.User.Identity.IsAuthenticated)
            {
                var userId = await _userService.GetUserIdAsync(HttpContext);
                ViewBag.UserId = userId;
                ViewBag.UserName = HttpContext.User.Identity.Name;
            }

            var country = await _breweryService.GetBrewery(beerDTO.BreweryId);
            var style = await _styleService.GetStyle(beerDTO.StyleId);
           
            var rating = await this._service.GetBeerRating((int)id);

            ViewBag.BeerCountry = country.Name;
            ViewBag.BeerStyle = style.StyleType;
            ViewBag.Rating = string.Format("{0:0.00}", rating);
            ViewBag.Reviews = await this._beerReviewService.GetReviewsByBeer((int)id);


            var beerView = new BeerViewModel(beerDTO);

            return View(beerView);
        }

        // GET: Beers/Create
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create()
        {


            ViewData["StyleId"] = new SelectList(await _styleService.GetAllStyles(), "Id", "StyleType");
            //ViewData["CreatedByUserId"] = new SelectList(_context.Users, "Id", "Name", beer.CreatedByUserId);
            ViewData["BreweryId"] = new SelectList(await _breweryService.GetAllBreweries(), "Id", "Name");

            return View();
        }

        // POST: Beers/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,Brand,ABV,StyleId,BreweryId")] BeerDTO beerDTO)
        {
            if (ModelState.IsValid)
            {
                await _service.Create(beerDTO);

                return RedirectToAction(nameof(Index));
            }

            ViewData["Styles"] = new SelectList(await _styleService.GetAllStyles(), "Id", "StyleType");
            ViewData["Breweries"] = new SelectList(await _breweryService.GetAllBreweries(), "Id", "Name");

            var beerView = new BeerViewModel(beerDTO);

            return View(beerView);
        }

        // GET: Beers/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beerDTO = await _service.GetBeer((int)id);

            if (beerDTO == null)
            {
                return NotFound();
            }
            ViewData["BreweryId"] = new SelectList(await _breweryService.GetAllBreweries(), "Id", "Name", beerDTO.BreweryId);
            //ViewData["CreatedByUserId"] = new SelectList(_context.Users, "Id", "Name", beer.CreatedByUserId);
            ViewData["StyleId"] = new SelectList(await _styleService.GetAllStyles(), "Id", "StyleType", beerDTO.StyleId);

            var beerView = new BeerViewModel(beerDTO);

            return View(beerView);
        }

        // POST: Beers/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, BeerDTO beerDTO)
        {
            if (id != beerDTO.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _service.Update(id, beerDTO);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BeerExists(beerDTO.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BreweryId"] = new SelectList(await _breweryService.GetAllBreweries(), "Id", "Name", beerDTO.BreweryId);
            //ViewData["CreatedByUserId"] = new SelectList(_context.Users, "Id", "Name", beer.CreatedByUserId);
            ViewData["StyleId"] = new SelectList(await _styleService.GetAllStyles(), "Id", "StyleType", beerDTO.StyleId);

            var beerView = new BeerViewModel(beerDTO);

            return View(beerView);
        }

        // GET: Beers/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beerDTO = await _service.GetBeer((int)id);

            if (beerDTO == null)
            {
                return NotFound();
            }

            var beerView = new BeerViewModel(beerDTO);

            return View(beerView);
        }

        // POST: Beers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var beer = await _service.Delete(id);

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> RateBeer(int beerId, int rating)
        {
            var userId = await this._userService.GetUserIdAsync(HttpContext);

            await _service.RateBeer(beerId, rating, userId);

            return RedirectToAction("Details", "Beers", new { id = beerId, msg = TempData["Msg"] = $"Your gave this one a {rating} !" });
        }

        private bool BeerExists(int id)
        {
            var beer = _service.GetBeer(id);

            if (beer != null)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> AddToWish(int? beerId)
        {
            if (beerId == null)
            {
                return NotFound();
            }

            var userId = await this._userService.GetUserIdAsync(HttpContext);

            var userBeer = new UserBeerRelationsDTO()
            {
                UserId = userId,
                BeerId = (int)beerId,
                Wish = true
            };

            var added = await this._userService.AddToWishList(userBeer);

            if (added == true)
            {
                return RedirectToAction("Details", "Beers", new { id = userBeer.BeerId, msg = TempData["Msg"] = "Beer added to Wishlist." });
            }
            else
            {
                return RedirectToAction("Details", "Beers", new { id = userBeer.BeerId, error = TempData["Error"] = "You cant add that beer to your wishlist again!" });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> AddToDrank(int? beerId)
        {
            if (beerId == null)
            {
                return NotFound();
            }

            var userId = await this._userService.GetUserIdAsync(HttpContext);

            var userBeer = new UserBeerRelationsDTO()
            {
                UserId = userId,
                BeerId = (int)beerId,
                Wish = true
            };

            var added = await this._userService.AddToDrankList(userBeer);

            if (added == true)
            {
                return RedirectToAction("DrankList", "Home", new { id = userBeer.BeerId, msg = TempData["Msg"] = "Beer added to Dranklist." });
            }
            else
            {
                return RedirectToAction("DrankList", "Home", new { id = userBeer.BeerId, error = TempData["Error"] = "You cant add that beer to your dranklist again!" });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> RemoveFromDrank(int? beerId)
        {
            if (beerId == null)
            {
                return NotFound();
            }

            var userId = await this._userService.GetUserIdAsync(HttpContext);

            var userBeer = new UserBeerRelationsDTO()
            {
                UserId = userId,
                BeerId = (int)beerId,
                Wish = false
            };

            var removed = await this._userService.RemoveFromDrankList(userBeer);

            if (removed == true)
            {
                return RedirectToAction("DrankList", "Home", new { id = userBeer.BeerId, msg = TempData["Msg"] = "Beer removed from the list." });
            }
            else
            {
                return RedirectToAction("DrankList", "Home", new { id = userBeer.BeerId, error = TempData["Error"] = "Something went wrong." });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> RemoveFromWish(int? beerId)
        {
            if (beerId == null)
            {
                return NotFound();
            }

            var userId = await this._userService.GetUserIdAsync(HttpContext);

            var userBeer = new UserBeerRelationsDTO()
            {
                UserId = userId,
                BeerId = (int)beerId,
                Wish = false
            };

            var removed = await this._userService.RemoveFromWishList(userBeer);

            if (removed == true)
            {
                return RedirectToAction("WishList", "Home", new { id = userBeer.BeerId, msg = TempData["Msg"] = "Beer removed from the list." });
            }
            else
            {
                return RedirectToAction("WishList", "Home", new { id = userBeer.BeerId, error = TempData["Error"] = "Something went wrong." });
            }
        }


    }
}
