﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Beers.Web.Models;
using Beers.Services.Contracts;

namespace Beers.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IBeerService beerService;
        private readonly IBreweryService breweryService;
        private readonly IBeerReviewService beerReviewService;
        private readonly IStyleService styleService;
        private readonly ICountryService countryService;
        private readonly IUserService _userService;

        public HomeController(ILogger<HomeController> logger,
                                IBeerService beerService,
                                IBreweryService breweryService,
                                IBeerReviewService beerReviewService,
                                IStyleService styleService,
                                ICountryService countryService,
                                IUserService userService
                             )
        {
            _logger = logger;
            this.beerService = beerService ?? throw new ArgumentNullException(nameof(beerService));
            this.breweryService = breweryService ?? throw new ArgumentNullException(nameof(breweryService));
            this.beerReviewService = beerReviewService ?? throw new ArgumentNullException(nameof(beerReviewService));
            this.styleService = styleService ?? throw new ArgumentNullException(nameof(styleService));
            this.countryService = countryService ?? throw new ArgumentNullException(nameof(countryService));
            this._userService = userService;
        }

        public async Task<IActionResult> Index()
        {
            var topRatedBeers = await this.beerService.TopThreeRatedBeers();

            var model = new HomeIndexViewModel
            {
                TopRatedBeers = topRatedBeers
                                    .Select(x => new BeerViewModel(x)).ToList()
            };

            return View(model);
        }
        [ActionName("Privacy")]
        public IActionResult Privacy()
        {
            return View();
        }

        [ActionName("Contacts")]
        public IActionResult Contacts()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> DrankList()
        {
            var drankBeers = await _userService.GetDrankList(await _userService.GetUserIdAsync(HttpContext));

            var beerModels = drankBeers.Select(x => new BeerViewModel(x)).ToList();

            var model = new DrankListViewModel() { DrankList = beerModels };

            return View(model);
        }

        public async Task<IActionResult> WishList()
        {
            var wishedBeers = await _userService.GetWishList(await _userService.GetUserIdAsync(HttpContext));

            var beerModels = wishedBeers.Select(x => new BeerViewModel(x)).ToList();

            var model = new WishListViewModel() { WishList = beerModels };

            return View(model);
        }
    }
}
