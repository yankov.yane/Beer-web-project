﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Beers.Data.Context;
using Beers.Models;
using Beers.Services.Contracts;
using Beers.Web.Models;
using Beers.Services.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace Beers.Web.Controllers
{
    public class StylesController : Controller
    {
        private readonly IStyleService _service;

        public StylesController(IStyleService service)
        {
            _service = service;
        }

        // GET: Styles
        public async Task<IActionResult> Index()
        {
            var stylesDTO = await _service.GetAllStyles();
            var styleViews = stylesDTO.Select(x => new StyleViewModel(x));

            return View(styleViews);
        }

        // GET: Styles/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var styleDTO = await _service.GetStyle((int)id);
            
            if (styleDTO == null)
            {
                return NotFound();
            }

            var styleView = new StyleViewModel(styleDTO);

            return View(styleView);
        }

        // GET: Styles/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Styles/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("StyleType")] StylesDTO styleDTO)
        {
            if (ModelState.IsValid)
            {
                await _service.Create(styleDTO);
                return RedirectToAction(nameof(Index));
            }

            var styleView = new StyleViewModel(styleDTO);

            return View(styleView);
        }

        // GET: Styles/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var styleDTO = await _service.GetStyle((int)id);
            if (styleDTO == null)
            {
                return NotFound();
            }

            var styleView = new StyleViewModel(styleDTO);

            return View(styleView);
        }

        // POST: Styles/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("StyleType")] StylesDTO styleDTO)
        {
            if (id != styleDTO.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _service.Update((int)id, styleDTO);
                }
                catch (DbUpdateConcurrencyException)
                {

                    if (!StyleExists(styleDTO.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            var styleView = new StyleViewModel(styleDTO);
            return View(styleView);
        }

        // GET: Styles/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var styleDTO = await _service.GetStyle((int)id);
            if (styleDTO == null)
            {
                return NotFound();
            }

            var styleView = new StyleViewModel(styleDTO);

            return View(styleView);
        }

        // POST: Styles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _service.Delete(id);

            return RedirectToAction(nameof(Index));
        }

        private bool StyleExists(int id)
        {
            var style =  _service.GetStyle(id);

            if (style == null)
            {
                return false;
            }

            return true;
        }
    }
}
