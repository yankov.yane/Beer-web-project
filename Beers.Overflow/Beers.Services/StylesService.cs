﻿using Beers.Data;
using Beers.Data.Context;
using Beers.Models;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beers.Services
{
    public class StylesService : IStyleService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeerOverflowContext _context;

        public StylesService(BeerOverflowContext context, IDateTimeProvider dateTimeProvider)
        {
            _context = context;
            this.dateTimeProvider = dateTimeProvider;
        }

        public async Task<StylesDTO> Create(StylesDTO stylesDTO)
        {
            if (stylesDTO == null)
            {
                throw new ArgumentException();
            }

            Style style = new Style
            {
                StyleType = stylesDTO.StyleType,
                CreatedOn = this.dateTimeProvider.GetDateTime()
            };

            await _context.Styles.AddAsync(style);
            await _context.SaveChangesAsync();

            return stylesDTO;
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var foundStyle = await _context.Styles.FirstOrDefaultAsync(x => x.Id == id);
                foundStyle.IsDeleted = true;
                foundStyle.ModifiedOn = this.dateTimeProvider.GetDateTime();

                await _context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                throw new ArgumentException();
            }
        }

        public async Task<IEnumerable<StylesDTO>> GetAllStyles()
        {
            var allStylesDTO = await _context.Styles
                                            .Where(x => x.IsDeleted == false)
                                            .Select(style => new StylesDTO(style))
                                            .ToListAsync();

            if (allStylesDTO == null)
            {
                throw new ArgumentException();
            }

            return allStylesDTO;
        }

        public async Task<StylesDTO> GetStyle(int id)
        {
            var foundStyle = await _context.Styles.FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);

            if (foundStyle == null)
            {
                throw new ArgumentException();
            }

            var foundStyleDTO = new StylesDTO(foundStyle);

            return foundStyleDTO;
        }

        public async Task<StylesDTO> Update(int id, StylesDTO styleDTO)
        {
            if (styleDTO == null)
            {
                throw new ArgumentException();
            }

            var foundStyle = await _context.Styles.FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);

            if (foundStyle == null)
            {
                throw new ArgumentException();
            }

            foundStyle.StyleType = styleDTO.StyleType;
            foundStyle.ModifiedOn = this.dateTimeProvider.GetDateTime();

            await _context.SaveChangesAsync();

            return new StylesDTO(foundStyle);
        }
    }
}
