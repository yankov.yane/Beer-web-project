﻿using Beers.Data;
using Beers.Data.Context;
using Beers.Models;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Services
{
    public class BeerReviewService : IBeerReviewService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeerOverflowContext _context;
        public BeerReviewService(BeerOverflowContext context, IDateTimeProvider dateTimeProvider)
        {
            _context = context;
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
        }

        public async Task<BeerReviewDTO> Create(BeerReviewDTO beerReviewDTO)
        {
            if (beerReviewDTO == null)
            {
                throw new ArgumentNullException();
            }

            var beerReview = new BeerReview
            {
                UserId = beerReviewDTO.UserId,
                BeerId = beerReviewDTO.BeerId,
                Content = beerReviewDTO.Content,
                UserName = beerReviewDTO.UserName,
                CreatedOn = this.dateTimeProvider.GetDateTime()
            };

            await _context.BeerReviews.AddAsync(beerReview);
            await _context.SaveChangesAsync();

            return beerReviewDTO;
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var review = _context.BeerReviews
                                    .Where(x => x.IsDeleted == false)
                                    .FirstOrDefault(x => x.Id == id);

                review.IsDeleted = true;
                review.DeletedOn = this.dateTimeProvider.GetDateTime();

                await _context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<BeerReviewDTO> GetById(int id)
        {
            var review = await _context.BeerReviews.FirstOrDefaultAsync(x => x.IsDeleted == false && x.Id == id);

            if (review == null)
            {
                throw new ArgumentNullException();
            }

            var reviewDTO = new BeerReviewDTO(review);

            return reviewDTO;
        }

        public async Task<IEnumerable<BeerReviewDTO>> GetAllReviews()
        {
            var reviews = await _context.BeerReviews
                .Where(x => x.IsDeleted == false)
                .Select(x => new BeerReviewDTO(x)).ToListAsync();

            return reviews;
        }

        public async Task<IEnumerable<BeerReviewDTO>> GetReviewsByBeer(int beerId)
        {
            var reviews = await _context.BeerReviews
                .Where(x => x.IsDeleted == false && x.BeerId == beerId)
                .Select(x => new BeerReviewDTO(x)).ToListAsync();

            foreach (var review in reviews)
            {
                review.Likes = await this.GetLikeCount(review.Id);
                review.Dislikes = await this.GetDislikeCount(review.Id);
            }

            return reviews;
        }

        public async Task<IEnumerable<BeerReviewDTO>> GetReviewsByUser(int userId)
        {
            var reviews = await _context.BeerReviews
                .Where(x => x.IsDeleted == false && x.UserId == userId)
                .Select(x => new BeerReviewDTO(x)).ToListAsync();

            foreach (var review in reviews)
            {
                review.Likes = await this.GetLikeCount(review.Id);
                review.Dislikes = await this.GetDislikeCount(review.Id);
            }

            return reviews;
        }

        public async Task<BeerReviewDTO> Update(int oldReviewId, BeerReviewDTO newBeerReviewDTO)
        {
            var oldReview = await _context.BeerReviews.Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Id == oldReviewId);

            if (oldReview == null || newBeerReviewDTO == null)
            {
                throw new ArgumentNullException();
            }

            oldReview.UserId = newBeerReviewDTO.UserId;
            oldReview.BeerId = newBeerReviewDTO.BeerId;
            oldReview.Content = newBeerReviewDTO.Content;
            oldReview.ModifiedOn = newBeerReviewDTO.ModifiedOn;
            oldReview.IsDeleted = newBeerReviewDTO.IsDeleted;
            oldReview.DeletedOn = newBeerReviewDTO.DeletedOn;
            oldReview.IsReported = newBeerReviewDTO.IsReported;

            await _context.SaveChangesAsync();

            return newBeerReviewDTO;
        }

        public async Task<int> GetLikeCount(int id)
        {

            var likeCount = await _context.BeerReviewVotes
                                            .Where(x => x.ReviewId == id && x.Like == true)
                                            .CountAsync();

            return likeCount;
        }

        public async Task<int> GetDislikeCount(int id)
        {
            var likeCount = await _context.BeerReviewVotes
                                            .Where(x => x.ReviewId == id && x.Like == false)
                                            .CountAsync();

            return likeCount;
        }

        public async Task<bool> Like(int reviewId, int userId)
        {
            var reviewVoteFound = await _context.BeerReviewVotes
                                    .FirstOrDefaultAsync(x => x.UserId == userId && x.ReviewId == reviewId);

            if (reviewVoteFound == null)
            {
                var reviewVote = new ReviewVote(reviewId, userId, true);
                await _context.BeerReviewVotes.AddAsync(reviewVote);
                await _context.SaveChangesAsync();
                return true;
            }

            if (reviewVoteFound.Like == true)
            {
                return false;
            }

            reviewVoteFound.Like = true;
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> Dislike(int reviewId, int userId)
        {
            var reviewVoteFound = await _context.BeerReviewVotes
                                    .FirstOrDefaultAsync(x => x.UserId == userId && x.ReviewId == reviewId);

            if (reviewVoteFound == null)
            {
                var reviewVote = new ReviewVote(reviewId, userId, false);
                await _context.BeerReviewVotes.AddAsync(reviewVote);
                await _context.SaveChangesAsync();
                return true;
            }

            if (reviewVoteFound.Like == false)
            {
                return false;
            }

            reviewVoteFound.Like = false;
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> Report(int id)
        {
            var review = await _context.BeerReviews.FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);

            if (review == null)
            {
                return false;
            }
            if (review.IsReported == true)
            {
                return false;
            }
            review.IsReported = true;
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> CheckIfReviewed(int id,int userId)
        {
            if (await this._context.BeerReviews.AnyAsync(x=>x.UserId == userId && x.BeerId == id))
            {
                return true;
            }

            return false;
        }
    }
}
