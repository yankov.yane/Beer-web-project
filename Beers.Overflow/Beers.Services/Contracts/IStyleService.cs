﻿using Beers.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Beers.Services.Contracts
{
    public interface IStyleService 
    {
        Task<StylesDTO> Create(StylesDTO stylesDTO);
        Task<StylesDTO> GetStyle(int id);
        Task<IEnumerable<StylesDTO>> GetAllStyles();
        Task<StylesDTO> Update(int id, StylesDTO style);
        Task<bool> Delete(int id);
    }
}