﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Beers.Services.DTOs;

namespace Beers.Services.Contracts
{
    public interface ICountryService
    {
        Task<CountryDTO> Create(CountryDTO countryDTO);
        Task<CountryDTO> GetCountry(int id);
        Task<IEnumerable<CountryDTO>> GetAllCountries();
        Task<CountryDTO> Update(int id, CountryDTO newCountry);
        Task<bool> Delete(int id);

    }
}
