﻿using Beers.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Beers.Services.Contracts
{
    public interface IBreweryService
    {
        Task<BreweryDTO> GetBrewery(int id);
        Task<IEnumerable<BreweryDTO>> GetAllBreweries();
        Task<BreweryDTO> Create(BreweryDTO breweryDTO);
        Task<BreweryDTO> Update(int id, BreweryDTO breweryDTO);
        public Task<bool> Delete(int id);
    }
}
