﻿using Beers.Models;
using Beers.Services.DTOs;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Beers.Services.Contracts
{
    public interface IUserService
    {
        Task<IList<User>> GetAllRegularUsers();
        Task<int> GetUserIdAsync(HttpContext http);

        Task<IEnumerable<BeerDTO>> GetDrankList(int userId);
        Task<IEnumerable<BeerDTO>> GetWishList(int userId);

        Task<bool> AddToWishList(UserBeerRelationsDTO userBeer);
        Task<bool> AddToDrankList(UserBeerRelationsDTO userBeer);

        Task<bool> RemoveFromDrankList(UserBeerRelationsDTO userBeer);
        Task<bool> RemoveFromWishList(UserBeerRelationsDTO userBeer);
        Task<bool> BanUserById(int id);
        Task<bool> UnbanUserById(int id);
    }
}
