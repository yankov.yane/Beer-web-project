﻿using Beers.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Beers.Services.Contracts
{
    public interface IBeerReviewService
    {
        Task<IEnumerable<BeerReviewDTO>> GetReviewsByUser(int UserId);
        Task<IEnumerable<BeerReviewDTO>> GetReviewsByBeer(int beerId);
        Task<BeerReviewDTO> Create(BeerReviewDTO beerReviewDTO);
        Task<BeerReviewDTO> Update(int id, BeerReviewDTO beerReviewDTO);
        Task<IEnumerable<BeerReviewDTO>> GetAllReviews();
        Task<BeerReviewDTO> GetById(int id);
        Task<bool> Delete(int id);
        Task<int> GetLikeCount(int id);
        Task<int> GetDislikeCount(int id);
        Task<bool> CheckIfReviewed(int id, int userId);
        Task<bool> Like(int reviewId, int userId);
        Task<bool> Dislike(int reviewId, int userId);
        Task<bool> Report(int id);
    }
}
