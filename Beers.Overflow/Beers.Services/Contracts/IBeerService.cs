﻿using Beers.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Beers.Services.Contracts
{
    public interface IBeerService
    {
        Task<BeerDTO> GetBeer(int id);
        Task<IEnumerable<BeerDTO>> GetAllBeers();
        Task<BeerDTO> Create(BeerDTO beerDTO);
        Task<BeerDTO> Update(int id, BeerDTO beerDTO);
        Task<bool> Delete(int id);
        Task<List<BeerDTO>> TopThreeRatedBeers();
        Task<decimal> GetBeerRating(int beerId);
        Task<decimal> RateBeer(int beerId, decimal rating, int userId);
    }
}
