﻿using System;

namespace Beers.Services.Providers.Contracts
{
    public interface IDateTimeProvider
    {
        DateTime GetDateTime();
    }
}
