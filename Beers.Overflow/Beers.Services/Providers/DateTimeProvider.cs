﻿using Beers.Services.Providers.Contracts;
using System;

namespace Beers.Services.Providers
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetDateTime() => DateTime.Now;
    }
}
