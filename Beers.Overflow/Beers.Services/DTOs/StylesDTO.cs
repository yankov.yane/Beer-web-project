﻿using Beers.Models;
using Beers.Services.DTOs.Abstraction;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beers.Services.DTOs
{
    public class StylesDTO : EntityDTO
    {
        public StylesDTO(Style style)
        {
            this.Id = style.Id;
            this.StyleType = style.StyleType;

            this.CreatedOn = style.CreatedOn;
            this.ModifiedOn = style.ModifiedOn;
            this.IsDeleted = style.IsDeleted;
            this.DeletedOn = style.DeletedOn;
        }

        public StylesDTO()
        {

        }

        public int Id { get; set; }
        public string StyleType { get; set; }
    }
}
