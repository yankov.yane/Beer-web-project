﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Beers.Services.DTOs.Abstraction
{
    public abstract class EntityDTO
    {
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
    }
}
