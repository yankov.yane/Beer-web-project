﻿using Beers.Models.Abstract;
using Beers.Services.DTOs.Abstraction;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beers.Services.DTOs
{
    public class UserBeerRelationsDTO : EntityDTO
    {
        public UserBeerRelationsDTO()
        {

        }

        public int UserId { get; set; }

        public int BeerId { get; set; }

        public bool Drank { get; set; }

        public bool Wish { get; set; }
    }
}
