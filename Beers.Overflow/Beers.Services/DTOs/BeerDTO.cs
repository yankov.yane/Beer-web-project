﻿using Beers.Models;
using Beers.Services.DTOs.Abstraction;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beers.Services.DTOs
{
    public class BeerDTO : EntityDTO
    {
        public BeerDTO(Beer beer)
        {
            this.Id = beer.Id;
            this.Name = beer.Name;
            this.Description = beer.Description;
            this.Brand = beer.Brand;
            this.ABV = beer.ABV;
            this.StyleId = beer.StyleId;
            this.BreweryId = beer.BreweryId;

            this.CreatedOn = beer.CreatedOn;
            this.ModifiedOn = beer.ModifiedOn;
            this.IsDeleted = beer.IsDeleted;
            this.DeletedOn = beer.DeletedOn;
        }

        public BeerDTO()
        {

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Brand { get; set; }
        public decimal ABV { get; set; }
        public int StyleId { get; set; }
        public int BreweryId { get; set; }
    }
}
