﻿using Beers.Models;
using Beers.Services.DTOs.Abstraction;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beers.Services.DTOs
{
    public class BreweryDTO : EntityDTO
    {
        public BreweryDTO(Brewery brewery)
        {
            this.Id = brewery.Id;
            this.Name = brewery.Name;
            this.Address = brewery.Address;
            this.CountryId = brewery.CountryId;
            this.CreatedOn = brewery.CreatedOn;
            this.ModifiedOn = brewery.ModifiedOn;
            this.IsDeleted = brewery.IsDeleted;
            this.DeletedOn = brewery.DeletedOn;
        }

        public BreweryDTO()
        {

        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int CountryId { get; set; }
    }
}
