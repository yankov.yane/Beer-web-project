﻿using Beers.Models;
using Beers.Services.DTOs.Abstraction;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beers.Services.DTOs
{
    public class CountryDTO : EntityDTO
    {
        public CountryDTO(Country country)
        {
            this.Id = country.Id;
            this.Name = country.Name;

            this.CreatedOn = country.CreatedOn;
            this.ModifiedOn = country.ModifiedOn;
            this.IsDeleted = country.IsDeleted;
            this.DeletedOn = country.DeletedOn;
        }

        public CountryDTO()
        {

        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}
