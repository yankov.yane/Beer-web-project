﻿using Beers.Models;
using Beers.Services.DTOs.Abstraction;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beers.Services.DTOs
{
    public class BeerReviewDTO : EntityDTO
    {
        public BeerReviewDTO(BeerReview review)
        {
            this.Id = review.Id;
            this.UserId = review.UserId;
            this.UserName = review.UserName;
            this.BeerId = review.BeerId;
            this.Content = review.Content;

            this.CreatedOn = review.CreatedOn;
            this.ModifiedOn = review.ModifiedOn;
            this.IsDeleted = review.IsDeleted;
            this.DeletedOn = review.DeletedOn;
            this.IsReported = review.IsReported;
        }
        public BeerReviewDTO()
        {

        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int BeerId { get; set; }
        public string Content { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
        public bool IsReported { get; set; }
    }
}
