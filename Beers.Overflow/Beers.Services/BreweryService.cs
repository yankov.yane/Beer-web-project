﻿using Beers.Data;
using Beers.Data.Context;
using Beers.Models;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beers.Services
{
    public class BreweryService : IBreweryService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeerOverflowContext _context;

        public BreweryService(BeerOverflowContext context, IDateTimeProvider dateTimeProvider)
        {
            _context = context;
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
        }

        public async Task<BreweryDTO> GetBrewery(int id)
        {
            var brewery = await _context.Breweries
                .Where(beer => beer.IsDeleted == false)
                .FirstOrDefaultAsync(beer => beer.Id == id);

            if (brewery == null)
            {
                throw new ArgumentNullException();
            }

            var breweryDTO = new BreweryDTO(brewery);

            return breweryDTO;
        }

        public async Task<IEnumerable<BreweryDTO>> GetAllBreweries()
        {
            var breweries = await _context.Breweries
                                        .Where(brewery => brewery.IsDeleted == false)
                                        .Select(brewery => new BreweryDTO(brewery))
                                        .ToListAsync();

            return breweries;
        }

        public async Task<BreweryDTO> Create(BreweryDTO breweryDTO)
        {
            if (breweryDTO.Name.Length < 2 || breweryDTO.Name.Length > 50)
            {
                throw new ArgumentOutOfRangeException("The beer name must be between 2 and 50 charectars!");
            }

            var brewery = new Brewery
            {
                Name = breweryDTO.Name,
                Address = breweryDTO.Address,
                CreatedOn = this.dateTimeProvider.GetDateTime(),
                CountryId = breweryDTO.CountryId
            };

            await _context.Breweries.AddAsync(brewery);
            await _context.SaveChangesAsync();

            return breweryDTO;
        }

        public async Task<BreweryDTO> Update(int id, BreweryDTO breweryDTO)
        {
            if (breweryDTO.Name.Length < 2 || breweryDTO.Name.Length > 50)
            {
                throw new ArgumentOutOfRangeException("The beer name must be between 2 and 50 charectars!");
            }

            var brewery = await _context.Breweries
                                        .Where(x => x.IsDeleted == false)
                                        .FirstOrDefaultAsync(x => x.Id == id);

            if (brewery == null)
            {
                throw new ArgumentNullException();
            }

            brewery.Name = breweryDTO.Name;
            brewery.Address = breweryDTO.Address;
            brewery.CountryId = breweryDTO.CountryId;
            brewery.ModifiedOn = this.dateTimeProvider.GetDateTime();

            await _context.SaveChangesAsync();

            return breweryDTO;
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var brewery = _context.Breweries
                                        .Where(x => x.IsDeleted == false)
                                        .FirstOrDefault(x => x.Id == id);

                brewery.IsDeleted = true;
                brewery.DeletedOn = this.dateTimeProvider.GetDateTime();

                await _context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
