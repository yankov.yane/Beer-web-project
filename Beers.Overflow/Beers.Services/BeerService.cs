﻿using Beers.Data;
using Beers.Data.Context;
using Beers.Models;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Services
{
    public class BeerService : IBeerService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeerOverflowContext _context;

        public BeerService(BeerOverflowContext context, IDateTimeProvider dateTimeProvider)
        {
            _context = context;
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
        }

        public async Task<BeerDTO> GetBeer(int id)
        {
            var beer = await _context.Beers
                                    .Include(x => x.Style)
                                    .Include(x => x.Brewery)
                                    .Where(beer => beer.IsDeleted == false)
                                    .FirstOrDefaultAsync(beer => beer.Id == id);

            if (beer == null)
            {
                throw new ArgumentNullException();
            }

            var beerDTO = new BeerDTO(beer);

            return beerDTO;
        }

        public async Task<IEnumerable<BeerDTO>> GetAllBeers()
        {
            var beers = await _context.Beers
                                        .Include(x => x.Style)
                                        .Include(x => x.Brewery)
                                        .Where(beer => beer.IsDeleted == false)
                                        .Select(beer => new BeerDTO(beer))
                                        .ToListAsync();

            return beers;
        }

        public async Task<BeerDTO> Create(BeerDTO beerDTO)
        {
            if (beerDTO.Name.Length < 2 || beerDTO.Name.Length > 50)
            {
                throw new ArgumentOutOfRangeException("The beer name must be between 2 and 50 charectars!");
            }
            if (beerDTO.Description.Length < 10 || beerDTO.Description.Length > 500)
            {
                throw new ArgumentOutOfRangeException("The beer's description must be between 10 and 500 characters!");
            }

            var beer = new Beer
            {
                Name = beerDTO.Name,
                Description = beerDTO.Description,
                ABV = beerDTO.ABV,
                Brand = beerDTO.Brand,
                StyleId = beerDTO.StyleId,
                BreweryId = beerDTO.BreweryId,
                CreatedOn = this.dateTimeProvider.GetDateTime()
            };

            await _context.Beers.AddAsync(beer);
            await _context.SaveChangesAsync();

            return beerDTO;
        }

        public async Task<BeerDTO> Update(int id, BeerDTO beerDTO)
        {
            if (beerDTO.Name.Length < 2 || beerDTO.Name.Length > 50)
            {
                throw new ArgumentOutOfRangeException("The beer name must be between 2 and 50 charectars!");
            }
            if (beerDTO.Description.Length < 10 || beerDTO.Description.Length > 500)
            {
                throw new ArgumentOutOfRangeException("The beer's description must be between 10 and 500 characters!");
            }

            var beer = await _context.Beers.Where(x => x.IsDeleted == false)
                                            .FirstOrDefaultAsync(x => x.Id == id);

            if (beer == null)
            {
                throw new ArgumentNullException();
            }

            beer.Name = beerDTO.Name;
            beer.Description = beerDTO.Description;
            beer.ABV = beerDTO.ABV;
            beer.Brand = beerDTO.Brand;
            beer.StyleId = beerDTO.StyleId;
            beer.BreweryId = beerDTO.BreweryId;
            beer.ModifiedOn = this.dateTimeProvider.GetDateTime();

            await _context.SaveChangesAsync();

            return beerDTO;
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var beer = _context.Beers
                                    .Where(x => x.IsDeleted == false)
                                    .FirstOrDefault(x => x.Id == id);

                beer.IsDeleted = true;

                beer.DeletedOn = this.dateTimeProvider.GetDateTime();

                await _context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<decimal> GetBeerRating(int beerId)
        {
            if (await _context.BeerRatings.AnyAsync(rating => rating.BeerId == beerId))
            {
                decimal rating = await _context.BeerRatings
                                            .Where(rating => rating.BeerId == beerId)
                                            .Select(rating => rating.Rating)
                                            .AverageAsync();
                return rating;
            }

            return 0.0m;
        }

        public async Task<List<BeerDTO>> TopThreeRatedBeers()
        {
            var beers = await _context.Beers
                                    .OrderByDescending(beer => beer.BeerRatings
                                    .Select(rating => rating.Rating).Average())
                                    .Take(3)
                                    .ToListAsync();

            var beerDTOs = beers.Select(beer => new BeerDTO(beer)).ToList();

            return beerDTOs;
        }

        public async Task<decimal> RateBeer(int beerId, decimal rating, int userId)
        {
            var rated = await this._context.BeerRatings.FirstOrDefaultAsync(x => x.BeerId == beerId && x.UserId == userId);
                
            if (rated != null)
            {
                rated.Rating = rating;
                await this._context.SaveChangesAsync();

                return rating;
            }

            var beerRating = new BeerRating()
            {
                BeerId = beerId,
                UserId = userId,
                Rating = rating
            };

            await this._context.BeerRatings.AddAsync(beerRating);
            await this._context.SaveChangesAsync();

            return rating;
        }

    }
}
