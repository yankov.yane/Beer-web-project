﻿using Beers.Data.Context;
using Beers.Models;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Beers.Services
{
    public class UserService : IUserService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeerOverflowContext _context;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> signInManager;

        public UserService(BeerOverflowContext context, IDateTimeProvider dateTimeProvider,
                            UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _context = context;
            this.dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
            this._userManager = userManager;
            this.signInManager = signInManager;
        }

        public async Task<IList<User>> GetAllRegularUsers()
        {
            var regularUsers = new List<User>();
            var users = this._context.Users.ToList();

            foreach (var u in users)
            {
                if (await this._userManager.IsInRoleAsync(u, "User"))
                {
                    regularUsers.Add(u);
                }
            }

            return regularUsers;

        }

        public async Task<int> GetUserIdAsync(HttpContext http)
        {
            var user = await _userManager.GetUserAsync(http.User);

            return user.Id;
        }

        public async Task<IEnumerable<BeerDTO>> GetDrankList(int userId)
        {
            var drankList = await this._context.UserBeerRelations
                                                 .Include(ub => ub.Beer)
                                                 .Where(ub => ub.UserId == userId && ub.Drank == true)
                                                 .Select(ub => ub.Beer).Select(beer => new BeerDTO(beer))
                                                 .ToListAsync();

            return drankList;
        }

        public async Task<IEnumerable<BeerDTO>> GetWishList(int userId)
        {
            var wishList = await this._context.UserBeerRelations
                                                 .Include(ub => ub.Beer)
                                                 .Where(ub => ub.UserId == userId && ub.Wish == true)
                                                 .Select(ub => ub.Beer).Select(beer => new BeerDTO(beer))
                                                 .ToListAsync();

            return wishList;
        }

        public async Task<bool> RemoveFromDrankList(UserBeerRelationsDTO userBeer)
        {
            var drankEntry = await this._context.UserBeerRelations
                                                 .FirstOrDefaultAsync(x =>
                                                 x.UserId == userBeer.UserId
                                                 && x.BeerId == userBeer.BeerId);
            if (drankEntry != null)
            {
                drankEntry.Drank = userBeer.Drank;
                await this._context.SaveChangesAsync();

                return true;
            }

            return false;
        }

        public async Task<bool> RemoveFromWishList(UserBeerRelationsDTO userBeer)
        {
            var wishEntry = await this._context.UserBeerRelations
                                                 .FirstOrDefaultAsync(x =>
                                                 x.UserId == userBeer.UserId
                                                 && x.BeerId == userBeer.BeerId);

            if (wishEntry != null)
            {
                wishEntry.Wish = userBeer.Wish;
                await this._context.SaveChangesAsync();

                return true;
            }

            return false;
        }

        public async Task<bool> AddToWishList(UserBeerRelationsDTO userBeer)
        {
            var userBeerEntry = this._context.UserBeerRelations
                                            .FirstOrDefaultAsync(x => x.UserId == userBeer.UserId
                                            && x.BeerId == userBeer.BeerId).Result;

            if (userBeerEntry == null)
            {
                var userBeerRelation = new UserBeerRelations()
                {
                    UserId = userBeer.UserId,
                    BeerId = userBeer.BeerId,
                    Wish = true
                };

                await this._context.UserBeerRelations.AddAsync(userBeerRelation);
                await this._context.SaveChangesAsync();

                return true;
            }
            if (userBeerEntry.Wish == false)
            {
                userBeerEntry.Wish = true;

                await this._context.SaveChangesAsync();

                return true;
            }

            return false;

        }

        public async Task<bool> AddToDrankList(UserBeerRelationsDTO userBeer)
        {
            var userBeerEntry = this._context.UserBeerRelations
                                           .FirstOrDefaultAsync(x => x.UserId == userBeer.UserId
                                           && x.BeerId == userBeer.BeerId).Result;

            if (userBeerEntry == null)
            {
                var userBeerRelation = new UserBeerRelations()
                {
                    UserId = userBeer.UserId,
                    BeerId = userBeer.BeerId,
                    Drank = true
                };

                await this._context.UserBeerRelations.AddAsync(userBeerRelation);
                await this._context.SaveChangesAsync();

                return true;
            }
            if (userBeerEntry.Drank == false)
            {
                userBeerEntry.Drank = true;

                await this._context.SaveChangesAsync();

                return true;
            }

            return false;
        }

        public async Task<bool> BanUserById(int id)
        {
            var userToBan = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);

            if (userToBan == null)
            {
                return false;
            }

            userToBan.LockoutEnabled = true;
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> UnbanUserById(int id)
        {
            var userToBan = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);

            if (userToBan == null)
            {
                return false;
            }

            userToBan.LockoutEnabled = false;
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
