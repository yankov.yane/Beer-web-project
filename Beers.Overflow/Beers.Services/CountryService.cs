﻿using System;
using System.Collections.Generic;
using System.Text;
using Beers.Services.Contracts;
using Beers.Services.DTOs;
using Beers.Data;
using Beers.Services.Providers.Contracts;
using Beers.Models;
using System.Linq;
using Beers.Data.Context;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Beers.Services
{
    public class CountryService : ICountryService
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly BeerOverflowContext _context;

        public CountryService(BeerOverflowContext context, IDateTimeProvider dateTimeProvider)
        {
            _context = context;
            this.dateTimeProvider = dateTimeProvider;
        }

        public async Task<CountryDTO> Create(CountryDTO countryDTO)
        {
            if (countryDTO == null)
            {
                throw new ArgumentException();
            }

            Country country = new Country
            {
                Name = countryDTO.Name,
                CreatedOn = this.dateTimeProvider.GetDateTime()
            };

            await _context.Countries.AddAsync(country);
            await _context.SaveChangesAsync();

            return countryDTO;
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var foundCountry = _context.Countries.FirstOrDefault(x => x.Id == id);
                foundCountry.IsDeleted = true;
                foundCountry.ModifiedOn = this.dateTimeProvider.GetDateTime();

                await _context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                throw new ArgumentException();
            }

        }

        public async Task<IEnumerable<CountryDTO>> GetAllCountries()
        {
            var allCountriesDTO = await _context.Countries
                                                .Where(x => x.IsDeleted == false)
                                                .Select(country => new CountryDTO(country))
                                                .ToListAsync();

            return allCountriesDTO;
        }

        public async Task<CountryDTO> GetCountry(int id)
        {
            var foundCountry = await _context.Countries.FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);

            if (foundCountry == null)
            {
                throw new ArgumentException();
            }

            var countryDTO = new CountryDTO(foundCountry);

            return countryDTO;
        }

        public async Task<CountryDTO> Update(int id, CountryDTO newCountry)
        {
            if (newCountry == null)
            {
                throw new ArgumentException();
            }

            var foundCountry = await _context.Countries.FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);

            if (foundCountry == null)
            {
                throw new ArgumentException();
            }

            foundCountry.Name = newCountry.Name;
            foundCountry.ModifiedOn = this.dateTimeProvider.GetDateTime();

            await _context.SaveChangesAsync();

            return new CountryDTO(foundCountry);
        }
    }
}
