﻿using Beers.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beers.Data.Configurations
{
    class ReviewVoteConfig : IEntityTypeConfiguration<ReviewVote>
    {
        public void Configure(EntityTypeBuilder<ReviewVote> builder)
        {
            builder.HasKey(x => x.Id);

            //many to many Likes to reviews
            builder.HasOne(vote => vote.Review)
                .WithMany(review => review.ReviewVotes)
                .HasForeignKey(vote => vote.ReviewId)
                .OnDelete(DeleteBehavior.Restrict);

            // many to many likes to users
            builder.HasOne(vote => vote.User)
                .WithMany(user => user.ReviewVotes)
                .HasForeignKey(vote => vote.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
