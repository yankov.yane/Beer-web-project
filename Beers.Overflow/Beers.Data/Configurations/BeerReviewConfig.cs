﻿using Beers.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Beers.Data.Configurations
{
    class BeerReviewConfig : IEntityTypeConfiguration<BeerReview>
    {
        public void Configure(EntityTypeBuilder<BeerReview> builder)
        {
            builder.HasKey(x => x.Id);

            //One to many BeerReviews - Beer 
            builder.HasOne(x => x.Beer)
                .WithMany(beer => beer.BeerReviews)
                .HasForeignKey(beer => beer.BeerId)
                .OnDelete(DeleteBehavior.Restrict);

            //One to many BeerReviews - User
            builder.HasOne(beerReview => beerReview.User)
                .WithMany(user => user.BeerReviews)
                .HasForeignKey(beerReview => beerReview.UserId)
                .OnDelete(DeleteBehavior.Restrict);

        }
    }
}