﻿using Beers.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Beers.Data.Configurations
{
    class BeerRatingConfig : IEntityTypeConfiguration<BeerRating>
    {
        public void Configure(EntityTypeBuilder<BeerRating> builder)
        {
            builder.HasKey(x => x.Id);

            //One to many BeerRating - Beer 
            builder.HasOne(beerRating => beerRating.Beer)
                .WithMany(beer => beer.BeerRatings)
                .HasForeignKey(beerRating => beerRating.BeerId)
                .OnDelete(DeleteBehavior.Restrict);

            //One to many BeerRating - User
            builder.HasOne(beerRating => beerRating.User)
                .WithMany(user => user.BeerRatings)
                .HasForeignKey(beerRating => beerRating.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
