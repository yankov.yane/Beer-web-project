﻿using Beers.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beers.Data.Configurations
{
    class BreweryConfig : IEntityTypeConfiguration<Brewery>
    {
        public void Configure(EntityTypeBuilder<Brewery> builder)
        {
            builder.HasKey(x => x.Id);

            //One to many Country - Breweries 
            builder.HasOne(brewery => brewery.Country)
                .WithMany(country => country.Breweries)
                .HasForeignKey(brewery => brewery.CountryId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
