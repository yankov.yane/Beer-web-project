﻿using Beers.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beers.Data.Configurations
{
    class BeerConfig : IEntityTypeConfiguration<Beer>
    {
        public void Configure(EntityTypeBuilder<Beer> builder)
        {
            builder.HasKey(x => x.Id);

            //One to many   Brewery - Beers
            builder.HasOne(beer => beer.Brewery)
                .WithMany(brewery => brewery.Beers)
                .HasForeignKey(beer => beer.BreweryId)
                .OnDelete(DeleteBehavior.Restrict);

            //One to many   Style - Beers
            builder.HasOne(beer => beer.Style)
                .WithMany(style => style.Beers)
                .HasForeignKey(beer => beer.StyleId)
                .OnDelete(DeleteBehavior.Restrict);

            //One to many   User - CreatedBeers
            builder.HasOne(beer => beer.CreatedByUser)
                .WithMany(user => user.CreatedBeers)
                .HasForeignKey(beer => beer.CreatedByUserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
