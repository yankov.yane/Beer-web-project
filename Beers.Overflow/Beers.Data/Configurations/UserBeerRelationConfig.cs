﻿using Beers.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Beers.Data.Configurations
{
    class UserBeerRelationConfig : IEntityTypeConfiguration<UserBeerRelations>
    {
        public void Configure(EntityTypeBuilder<UserBeerRelations> builder)
        {
            builder.HasKey(e => new { e.BeerId, e.UserId });

            //One to many UserBeerRelations - Beer 
            builder.HasOne(ubrelation => ubrelation.Beer)
                .WithMany(beer => beer.UserBeerRelations)
                .HasForeignKey(beerRating => beerRating.BeerId)
                .OnDelete(DeleteBehavior.Restrict);

            //One to many UserBeerRelations - User
            builder.HasOne(ubrelation => ubrelation.User)
                .WithMany(user => user.UserBeerRelations)
                .HasForeignKey(ubrelation => ubrelation.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
