﻿using Beers.Data.Configurations;
using Beers.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Beers.Data.Context
{
    public class BeerOverflowContext : IdentityDbContext<User, Role, int>
    {
        public DbSet<Country> Countries { get; set; }
        public DbSet<Beer> Beers { get; set; }
        public DbSet<Brewery> Breweries { get; set; }
        public DbSet<Style> Styles { get; set; }
        public DbSet<BeerRating> BeerRatings { get; set; }
        public DbSet<BeerReview> BeerReviews { get; set; }
        public DbSet<ReviewVote> BeerReviewVotes { get; set; }
        public DbSet<UserBeerRelations> UserBeerRelations { get; set; }

        public BeerOverflowContext(DbContextOptions<BeerOverflowContext> options)
            : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new CountryConfig());
            builder.ApplyConfiguration(new BeerConfig());
            builder.ApplyConfiguration(new BreweryConfig());
            builder.ApplyConfiguration(new BeerRatingConfig());
            builder.ApplyConfiguration(new BeerReviewConfig());


            builder.Entity<Role>().HasData(
               new Role() { Id = 1, Name = "Admin", NormalizedName = "ADMIN" },
               new Role() { Id = 2, Name = "User", NormalizedName = "USER" }
            );

            // admin account
            var hasher = new PasswordHasher<User>();
            var adminUser = new User();
            adminUser.Id = 3;
            adminUser.UserName = "admin@admin.com";
            adminUser.NormalizedUserName = "ADMIN@ADMIN.COM";
            adminUser.Email = "admin@admin.com";
            adminUser.NormalizedEmail = "ADMIN@ADMIN.COM";
            adminUser.PasswordHash = hasher.HashPassword(adminUser, "admin123");
            adminUser.SecurityStamp = Guid.NewGuid().ToString();
            builder.Entity<User>().HasData(adminUser);
            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>()
                {
                    RoleId = 1,
                    UserId = adminUser.Id
                }
            );

            // user accounts
            // Alice
            var regularUser = new User();
            regularUser.Id = 4;
            regularUser.UserName = "alice@alice.com";
            regularUser.NormalizedUserName = "ALICE@ALICE.COM";
            regularUser.Email = "alice@alice.com";
            regularUser.NormalizedEmail = "ALICE@ALICE.COM";
            regularUser.PasswordHash = hasher.HashPassword(regularUser, "alice123");
            regularUser.SecurityStamp = Guid.NewGuid().ToString();
            builder.Entity<User>().HasData(regularUser);
            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>()
                {
                    RoleId = 2,
                    UserId = regularUser.Id
                }
            );
            // sirus
            regularUser = new User();
            regularUser.Id = 5;
            regularUser.UserName = "sirus@bob.com";
            regularUser.NormalizedUserName = "sirus@BOB.COM";
            regularUser.Email = "sirus@bob.com";
            regularUser.NormalizedEmail = "SIRUS@BOB.COM";
            regularUser.PasswordHash = hasher.HashPassword(regularUser, "sirus123");
            regularUser.SecurityStamp = Guid.NewGuid().ToString();
            builder.Entity<User>().HasData(regularUser);
            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>()
                {
                    RoleId = 2,
                    UserId = regularUser.Id
                }
            );

            Beer[] beers = new Beer[]
            {
                new Beer()
                {
                    CreatedOn = DateTime.Now,
                    Id = 1,
                    Name = "American Amber Ale",
                    Description = "Like most amber beers, American amber ale is named after the golden to amber color this American version...",
                    BreweryId = 1,
                    ABV = 4.4M,
                    StyleId = 1,
                    Brand = "Bud-Light"

                },
                new Beer()
                {
                    CreatedOn = DateTime.Now,
                    Id = 2,
                    Name = "American Amber Lager",
                    Description = "A widely available, sessionable craft beer style that showcases both malt and hops. Amber lagers are...",
                    BreweryId = 1,
                    ABV = 4.8M,
                    StyleId = 2,
                    Brand = "Bud-Light"

                },
                new Beer()
                {
                    CreatedOn = DateTime.Now,
                    Id = 3,
                    Name = "American Black Ale",
                    Description = "The American black ale is characterized by the perception of caramel malt and dark roasted malt flavor...",
                    BreweryId = 1,
                    ABV = 6M,
                    StyleId = 1,
                    Brand = "Bud-Light"

                },
                new Beer()
                {
                    CreatedOn = DateTime.Now,
                    Id = 4,
                    Name = "Belgian-Style Witbier",
                    Description = "Belgian-style witbier is brewed using unmalted wheat, sometimes oats and malted barley. Witbiers are spiced with coriander and orange peel. A style that dates back hundreds of years,",
                    BreweryId = 2,
                    ABV = 4.8M,
                    StyleId = 3,
                    Brand = "San Miguel"

                },
                new Beer()
                {
                    CreatedOn = DateTime.Now,
                    Id = 5,
                    Name = "Belgian-Style Tripel",
                    Description = "Complex, sometimes mild spicy flavor characterizes this style. Yeast-driven complexity is common. Tripels are often on the higher end of the ABV spectrum, ",
                    BreweryId = 2,
                    ABV = 7.1M,
                    StyleId = 4,
                    Brand = "Corona Extra"

                },
};


            Country[] countries = new Country[]
            {
                new Country()
                {
                    Id = 1,
                    Name = "Bulgaria",
                    CreatedOn = DateTime.Now.AddYears(-5)
                },
                new Country()
                {
                    Id = 2,
                    Name = "United Kingsoms",
                    CreatedOn = DateTime.Now.AddYears(-5)
                },
                new Country()
                {
                    Id = 3,
                    Name = "Serbia",
                    CreatedOn = DateTime.Now.AddYears(-5)
                }
            };

            Brewery[] breweries = new Brewery[]
            {
                new Brewery()
                {
                    Id = 1,
                    Name = "Grillin & Chillin Alehouse",
                    Address = "6310 Southside Road Hollister CA 95023",
                    CountryId = 1
                },
                new Brewery()
                {
                    Id = 2,
                    Name = "Brewery Twenty Five",
                    Address = "401 McCray St b24, Hollister, CA 95023",
                    CountryId = 2
                },
                new Brewery()
                {
                    Id = 3,
                    Name = "Suncoast Nuthouse Brew",
                    Address = "106 Third St, San Juan Bautista, CA 95045, United States",
                    CountryId = 3
                }
            };

            Style[] styles = new Style[]
            {
            new Style()
            {
                Id = 1,
                StyleType = "Ale"
            },
            new Style()
            {
                Id = 2,
                StyleType = "Lager"
            },
            new Style()
            {
                Id = 3,
                StyleType = "Stout"
            },
            new Style()
            {
                Id = 4,
                StyleType = "Pilsner"
            },
            new Style()
            {
                Id = 5,
                StyleType = "Bitter"
            },


            };

            //User user = new User()
            //{
            //    Id = 1,
            //    Name = "Kiro";
            //};

            //BeerRating[] ratings = new BeerRating[]
            //{
            //new BeerRating
            //{
            //    BeerId = 1,
            //    UserId = 1,
            //    Rating = 5
            //},
            //new BeerRating
            //{
            //    BeerId = 2,
            //    UserId = 1,
            //    Rating = 5
            //},
            //new BeerRating
            //{
            //    BeerId = 3,
            //    UserId = 1,
            //    Rating = 5
            //}
            //};

            builder.Entity<Country>().HasData(countries);

            //builder.Entity<User>().HasData(user);

            builder.Entity<Style>().HasData(styles);

            builder.Entity<Beer>().HasData(beers);

            builder.Entity<Brewery>().HasData(breweries);

            // builder.Entity<BeerRating>().HasData(ratings);


            ////For seeding the database
            //var seeder = new Seed();
            //seeder.AddData(builder);

            base.OnModelCreating(builder);
        }
    }
}
