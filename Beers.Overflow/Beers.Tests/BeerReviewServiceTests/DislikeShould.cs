﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace Beers.Tests.BeerReviewServiceTests
{
    [TestClass]
    public class DislikeShould
    {
        [TestMethod]
        public async Task DislikeCorrectly()
        {
            var options = Utils.GetOptions(nameof(DislikeCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();

            var beerReviewDTO1 = new BeerReviewDTO
            {
                UserId = 1,
                BeerId = 1,
                Content = "Good review",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false,
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(arrangeContext, providerMock.Object);
                var review = await sut.Create(beerReviewDTO1);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerReviewService(assertContext, providerMock.Object);
                var result = await sut.Dislike(0, 1);

                Assert.AreEqual(result, true);

            }
        }

        [TestMethod]
        public async Task DislikeReturnsFalseWhenAreadyDisliked()
        {
            var options = Utils.GetOptions(nameof(DislikeReturnsFalseWhenAreadyDisliked));

            var providerMock = new Mock<IDateTimeProvider>();

            var beerReviewDTO1 = new BeerReviewDTO
            {
                UserId = 1,
                BeerId = 1,
                Content = "Good review",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false,
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(arrangeContext, providerMock.Object);
                var review = await sut.Create(beerReviewDTO1);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerReviewService(assertContext, providerMock.Object);
                var result = await sut.Dislike(0, 1);
                var result2 = await sut.Dislike(0, 1);

                Assert.AreEqual(result2, false);

            }
        }

    }
}
