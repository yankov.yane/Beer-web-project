﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Tests.BeerReviewServiceTests
{
    [TestClass]
    public class CreateShould
    {
        [TestMethod]
        public async Task CreateBeerReviewCorrectly()
        {
            var options = Utils.GetOptions(nameof(CreateBeerReviewCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();
            var beerReviewDTO = new BeerReviewDTO
            {
                UserId = 1,
                BeerId = 1,
                Content = "Good review",
                UserName = "Admin",
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(arrangeContext, providerMock.Object);
                var review = await sut.Create(beerReviewDTO);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var result = await assertContext.BeerReviews.FirstOrDefaultAsync(x => x.Id == 1);

                Assert.AreEqual(result.UserId, 1);
                Assert.AreEqual(result.BeerId, 1);
                Assert.AreEqual(result.Content, "Good review");
                Assert.AreEqual(result.UserName, "Admin");
                Assert.AreEqual(result.ModifiedOn, null);
                Assert.AreEqual(result.IsDeleted, false);
                Assert.AreEqual(result.DeletedOn, null);
                Assert.AreEqual(result.IsReported, false);
            }
        }

        [TestMethod]
        public async Task CreateBeerReviewThrowsWhenNull()
        {
            var options = Utils.GetOptions(nameof(CreateBeerReviewThrowsWhenNull));

            var providerMock = new Mock<IDateTimeProvider>();
            BeerReviewDTO beerReviewDTO = null;

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(arrangeContext, providerMock.Object);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.Create(beerReviewDTO));
            }
        }
    }
}
