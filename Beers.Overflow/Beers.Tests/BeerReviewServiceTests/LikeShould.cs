﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace Beers.Tests.BeerReviewServiceTests
{
    [TestClass]
    public class LikeShould
    {

        [TestMethod]
        public async Task LikeCorrectly()
        {
            var options = Utils.GetOptions(nameof(LikeCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();

            var beerReviewDTO1 = new BeerReviewDTO
            {
                UserId = 1,
                BeerId = 1,
                Content = "Good review",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false,
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(arrangeContext, providerMock.Object);
                var review = await sut.Create(beerReviewDTO1);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerReviewService(assertContext, providerMock.Object);
                var result = await sut.Like(0, 1);

                Assert.AreEqual(result, true);

            }
        }

        [TestMethod]
        public async Task LikeReturnsFalseWhenAreadyLiked()
        {
            var options = Utils.GetOptions(nameof(LikeReturnsFalseWhenAreadyLiked));

            var providerMock = new Mock<IDateTimeProvider>();

            var beerReviewDTO1 = new BeerReviewDTO
            {
                UserId = 1,
                BeerId = 1,
                Content = "Good review",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false,
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(arrangeContext, providerMock.Object);
                var review = await sut.Create(beerReviewDTO1);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerReviewService(assertContext, providerMock.Object);
                var result = await sut.Like(0, 1);
                var result2 = await sut.Like(0, 1);

                Assert.AreEqual(result2, false);

            }
        }

    }
}
