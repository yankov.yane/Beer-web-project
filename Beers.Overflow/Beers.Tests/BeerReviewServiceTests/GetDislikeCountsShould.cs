﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace Beers.Tests.BeerReviewServiceTests
{
    [TestClass]
    public class GetDislikeCountsShould
    {
        [TestMethod]
        public async Task GetDislikeCountCorrectly()
        {
            var options = Utils.GetOptions(nameof(GetDislikeCountCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();

            var beerReviewDTO1 = new BeerReviewDTO
            {
                UserId = 1,
                BeerId = 1,
                Content = "Good review",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false,
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(arrangeContext, providerMock.Object);
                var review = await sut.Create(beerReviewDTO1);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerReviewService(assertContext, providerMock.Object);
                var result1 = await sut.Dislike(0, 1);
                var result2 = await sut.Dislike(0, 2);
                var result3 = await sut.Dislike(0, 3);
                var result4 = await sut.Like(0, 4);

                var actual = await sut.GetDislikeCount(0);

                Assert.AreEqual(actual, 3);

            }
        }
    }
}
