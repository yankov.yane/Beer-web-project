﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Tests.BeerReviewServiceTests
{
    [TestClass]
    public class GetReviewsByBeerSould
    {
        [TestMethod]
        public async Task GetReviewsByBeerCorrectlyWith1Review()
        {
            var options = Utils.GetOptions(nameof(GetReviewsByBeerCorrectlyWith1Review));

            var providerMock = new Mock<IDateTimeProvider>();


            var beerReviewDTO1 = new BeerReviewDTO
            {
                UserId = 1,
                BeerId = 1,
                Content = "Good review",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false
            };
            var beerReviewDTO2 = new BeerReviewDTO
            {
                UserId = 2,
                BeerId = 1,
                Content = "Best review",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false
            }; 
            var beerReviewDTO3 = new BeerReviewDTO
            {
                UserId = 2,
                BeerId = 2,
                Content = "Awesome review",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(arrangeContext, providerMock.Object);
                var review = await sut.Create(beerReviewDTO1);
                var review2 = await sut.Create(beerReviewDTO2);
                var review3 = await sut.Create(beerReviewDTO3);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerReviewService(assertContext, providerMock.Object);
                var result = await sut.GetReviewsByBeer(2);

                Assert.AreEqual(result.Count(), 1);

            }
        }

        [TestMethod]
        public async Task GetReviewsByBeerCorrectlyWith3Reviews()
        {
            var options = Utils.GetOptions(nameof(GetReviewsByBeerCorrectlyWith3Reviews));

            var providerMock = new Mock<IDateTimeProvider>();


            var beerReviewDTO1 = new BeerReviewDTO
            {
                UserId = 1,
                BeerId = 2,
                Content = "Good review",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false
            };
            var beerReviewDTO2 = new BeerReviewDTO
            {
                UserId = 4,
                BeerId = 2,
                Content = "Best review",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false
            };
            var beerReviewDTO3 = new BeerReviewDTO
            {
                UserId = 2,
                BeerId = 2,
                Content = "Awesome review",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(arrangeContext, providerMock.Object);
                var review = await sut.Create(beerReviewDTO1);
                var review2 = await sut.Create(beerReviewDTO2);
                var review3 = await sut.Create(beerReviewDTO3);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerReviewService(assertContext, providerMock.Object);
                var result = await sut.GetReviewsByBeer(2);

                Assert.AreEqual(result.Count(), 3);

            }
        }

        [TestMethod]
        public async Task GetReviewsByBeerReturnsEmptyWhenNoDataOnDB()
        {
            var options = Utils.GetOptions(nameof(GetReviewsByBeerReturnsEmptyWhenNoDataOnDB));
            var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerReviewService(assertContext, providerMock.Object);
                var result = await sut.GetReviewsByBeer(2);

                Assert.AreEqual(result.Count(), 0);

            }
        }
    }
}
