﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Tests.BeerReviewServiceTests
{
    [TestClass]
    public class DeleteShould
    {
        [TestMethod]
        public async Task DeleteBeerReviewCorrectly()
        {
            var options = Utils.GetOptions(nameof(DeleteBeerReviewCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();
            var beerReviewDTO = new BeerReviewDTO
            {
                UserId = 1,
                BeerId = 1,
                Content = "Good review",
                UserName = "Admin",
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false
            };

            bool deletedIsOk = false;

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(arrangeContext, providerMock.Object);
                var review = await sut.Create(beerReviewDTO);
                await arrangeContext.SaveChangesAsync();

                deletedIsOk = await sut.Delete(1);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var result = await assertContext.BeerReviews.FirstOrDefaultAsync(x => x.Id == 1);

                Assert.AreEqual(result.Id, 1);
                Assert.AreEqual(result.IsDeleted, true);
                Assert.IsTrue(deletedIsOk);
            }
        }

        [TestMethod]
        public async Task DeleteBeerReviewReturnsFalseWhenNotFound()
        {
            var options = Utils.GetOptions(nameof(DeleteBeerReviewReturnsFalseWhenNotFound));

            var providerMock = new Mock<IDateTimeProvider>();
            var beerReviewDTO = new BeerReviewDTO
            {
                UserId = 1,
                BeerId = 1,
                Content = "Good review",
                UserName = "Admin",
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false
            };

            bool deletedIsOk = false;

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(arrangeContext, providerMock.Object);
                var review = await sut.Create(beerReviewDTO);

                deletedIsOk = await sut.Delete(5);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var result = await assertContext.BeerReviews.FirstOrDefaultAsync(x => x.Id == 1);

                Assert.AreEqual(result.Id, 1);
                Assert.AreEqual(result.IsDeleted, false);
                Assert.IsFalse(deletedIsOk);
            }
        }

    }
}
