﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Tests.BeerReviewServiceTests
{
    [TestClass]
    public class GetAllReviewsSould
    {
        [TestMethod]
        public async Task GetAllReviewsBeerReviewCorrectly()
        {
            var options = Utils.GetOptions(nameof(GetAllReviewsBeerReviewCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();
            var beerReviewDTO1 = new BeerReviewDTO
            {
                UserId = 1,
                BeerId = 1,
                Content = "Good review",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false
            };
            var beerReviewDTO2 = new BeerReviewDTO
            {
                UserId = 1,
                BeerId = 2,
                Content = "Best review",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(arrangeContext, providerMock.Object);
                var review = await sut.Create(beerReviewDTO1);
                var review2 = await sut.Create(beerReviewDTO2);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerReviewService(assertContext, providerMock.Object);
                var result = await sut.GetAllReviews();

                Assert.AreEqual(result.Count(), 2);
                foreach (var item in result)
                {
                    Assert.AreEqual(item.UserId, 1);
                    Assert.AreEqual(item.ModifiedOn, null);
                    Assert.AreEqual(item.IsDeleted, false);
                    Assert.AreEqual(item.DeletedOn, null);
                    Assert.AreEqual(item.IsReported, false);
                }

            }
        }

        [TestMethod]
        public async Task GetAllReviewsBeerReviewReturnsEmptyWhenNoDataOnDB()
        {
            var options = Utils.GetOptions(nameof(GetAllReviewsBeerReviewReturnsEmptyWhenNoDataOnDB));

            var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerReviewService(assertContext, providerMock.Object);
                var result = await sut.GetAllReviews();

                Assert.AreEqual(result.Count(), 0);

            }
        }
    }
}
