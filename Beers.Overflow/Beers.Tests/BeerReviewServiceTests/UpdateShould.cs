﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beers.Tests.BeerReviewServiceTests
{
    [TestClass]
    public class UpdateShould
    {
        [TestMethod]
        public async Task UpdateBeerReviewCorrectly()
        {
            var options = Utils.GetOptions(nameof(UpdateBeerReviewCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();
            var beerReviewDTO = new BeerReviewDTO
            {
                UserId = 1,
                BeerId = 1,
                Content = "Good review",
                UserName = "Admin",
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(arrangeContext, providerMock.Object);
                var review = await sut.Create(beerReviewDTO);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var beerReviewDTOToUpdate = new BeerReviewDTO
                {
                    UserId = 1,
                    BeerId = 1,
                    Content = "New text to replace the review",
                    UserName = "Admin",
                    CreatedOn = DateTime.Now,
                    ModifiedOn = null,
                    IsDeleted = false,
                    DeletedOn = null,
                    IsReported = false
                };

                //Act
                var sut = new BeerReviewService(assertContext, providerMock.Object);
                var expected = await sut.Update(1, beerReviewDTOToUpdate);
                await assertContext.SaveChangesAsync();
                var actual = await sut.GetById(1);

                Assert.AreEqual(actual.UserId, expected.UserId);
                Assert.AreEqual(actual.BeerId, expected.BeerId);
                Assert.AreEqual(actual.Content, expected.Content);
                Assert.AreEqual(actual.ModifiedOn, expected.ModifiedOn);
                Assert.AreEqual(actual.IsDeleted, expected.IsDeleted);
                Assert.AreEqual(actual.DeletedOn, expected.DeletedOn);
                Assert.AreEqual(actual.IsReported, expected.IsReported);
            }
        }

        [TestMethod]
        public async Task UpdateBeerReviewThrowsWhenReviewNotFound()
        {
            var options = Utils.GetOptions(nameof(UpdateBeerReviewThrowsWhenReviewNotFound));

            var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                var beerReviewDTOToUpdate = new BeerReviewDTO
                {
                    UserId = 1,
                    BeerId = 1,
                    Content = "Even better review",
                    ModifiedOn = null,
                    IsDeleted = false,
                    DeletedOn = null,
                    IsReported = false
                };

                //Act
                var sut = new BeerReviewService(assertContext, providerMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.Update(0, beerReviewDTOToUpdate));
            }
        }


        [TestMethod]
        public async Task UpdateBeerReviewThrowsWhenNoReviewGiven()
        {
            var options = Utils.GetOptions(nameof(UpdateBeerReviewThrowsWhenNoReviewGiven));

            var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerReviewService(assertContext, providerMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.Update(0, null));
            }
        }
    }
}
