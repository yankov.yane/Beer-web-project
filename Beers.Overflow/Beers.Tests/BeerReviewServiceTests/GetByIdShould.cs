﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace Beers.Tests.BeerReviewServiceTests
{
    [TestClass]
    public class GetByIdShould
    {
        [TestMethod]
        public async Task GetByIdBeerReviewCorrectly()
        {
            var options = Utils.GetOptions(nameof(GetByIdBeerReviewCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();
            var beerReviewDTO = new BeerReviewDTO
            {
                UserId = 1,
                BeerId = 1,
                Content = "Good review",
                UserName = "Admin",
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(arrangeContext, providerMock.Object);
                var review = await sut.Create(beerReviewDTO);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerReviewService(assertContext, providerMock.Object);
                var result = await sut.GetById(1);

                Assert.AreEqual(result.Id, 1);
                Assert.AreEqual(result.UserId, 1);
                Assert.AreEqual(result.BeerId, 1);
                Assert.AreEqual(result.Content, "Good review");
                Assert.AreEqual(result.ModifiedOn, null);
                Assert.AreEqual(result.IsDeleted, false);
                Assert.AreEqual(result.DeletedOn, null);
                Assert.AreEqual(result.IsReported, false);
            }
        }

        [TestMethod]
        public async Task GetByIdBeerReviewThrowsWhenNotFound()
        {
            var options = Utils.GetOptions(nameof(GetByIdBeerReviewThrowsWhenNotFound));

            var providerMock = new Mock<IDateTimeProvider>();
            var beerReviewDTO = new BeerReviewDTO
            {
                UserId = 1,
                BeerId = 1,
                Content = "Good review",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(arrangeContext, providerMock.Object);
                var review = await sut.Create(beerReviewDTO);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerReviewService(assertContext, providerMock.Object);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetById(2));
            }
        }


    }
}
