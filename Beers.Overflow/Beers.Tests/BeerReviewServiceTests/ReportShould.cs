﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace Beers.Tests.BeerReviewServiceTests
{
    [TestClass]
    public class ReportShould
    {
        [TestMethod]
        public async Task ReportCorrectly()
        {
            var options = Utils.GetOptions(nameof(ReportCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();

            var beerReviewDTO = new BeerReviewDTO
            {
                UserId = 1,
                BeerId = 1,
                Content = "Good review",
                UserName = "Admin",
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(arrangeContext, providerMock.Object);
                var review = await sut.Create(beerReviewDTO);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerReviewService(assertContext, providerMock.Object);
                var result = await sut.Report(1);

                Assert.AreEqual(result, true);

            }
        }

        [TestMethod]
        public async Task ReportReturnsFlaseWhenAlreadyReported()
        {
            var options = Utils.GetOptions(nameof(ReportReturnsFlaseWhenAlreadyReported));

            var providerMock = new Mock<IDateTimeProvider>();

            var beerReviewDTO1 = new BeerReviewDTO
            {
                UserId = 1,
                BeerId = 1,
                Content = "Good review",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                IsReported = false,
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerReviewService(arrangeContext, providerMock.Object);
                var review = await sut.Create(beerReviewDTO1);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerReviewService(assertContext, providerMock.Object);
                var result = await sut.Report(0);
                var result2 = await sut.Report(0);

                Assert.AreEqual(result2, false);

            }
        }

    }
}
