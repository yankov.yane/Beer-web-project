﻿using Beers.Data.Context;
using Beers.Models;
using Beers.Services;
using Beers.Services.Providers.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace Beers.Tests.BeerServiceTests
{
    [TestClass]
    public class GetBrewery_Should
    {
        [TestMethod]
        public async Task ReturnBrewery_Correctly()
        {
            var options = Utils.GetOptions(nameof(ReturnBrewery_Correctly));

            var providerMock = new Mock<IDateTimeProvider>();

            var brewery = new Brewery
            {
                Id = 1,
                Name = "Kamenitza"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Breweries.AddAsync(brewery);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BreweryService(assertContext, providerMock.Object);

                var result = await sut.GetBrewery(1);

                //Assert
                Assert.IsTrue(result.Name == brewery.Name);
            }
        }

        [TestMethod]
        public async Task ThrowExeption_OnBreweryNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowExeption_OnBreweryNotFound));

            var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerService(assertContext, providerMock.Object);

                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetBeer(1));
            }
        }
    }
}
