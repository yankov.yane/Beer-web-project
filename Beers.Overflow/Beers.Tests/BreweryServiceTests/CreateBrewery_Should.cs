﻿using Beers.Data.Context;
using Beers.Models;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Tests.BreweryServiceTests
{
    [TestClass]
    public class CreateBrewery_Should
    {
        [TestMethod]
        public async Task CreateBrewery_Correctly()
        {
            var options = Utils.GetOptions(nameof(CreateBrewery_Correctly));

            var providerMock = new Mock<IDateTimeProvider>();
            var breweryDTO = new BreweryDTO
            {
                Id = 1,
                Name = "Shumensko",
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BreweryService(arrangeContext, providerMock.Object);
                var brewery = sut.Create(breweryDTO);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var result = await assertContext.Breweries.FirstOrDefaultAsync(x => x.Id == breweryDTO.Id);

                //Assert
                Assert.IsTrue(result.Name == breweryDTO.Name);
            }
        }

        [TestMethod]
        public async Task ThrowExeptionOn_IncorrectName()
        {
            var options = Utils.GetOptions(nameof(ThrowExeptionOn_IncorrectName));

            var providerMock = new Mock<IDateTimeProvider>();

            var breweryDTO = new BreweryDTO
            {
                Id = 1,
                Name = "A",
            };

            using (var actContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BreweryService(actContext, providerMock.Object);

                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.Create(breweryDTO));
            }
        }
    }
}
