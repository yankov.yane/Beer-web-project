﻿using Beers.Data.Context;
using Beers.Models;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beers.Tests.BeerServiceTests
{
    [TestClass]
    public class UpdateBrewery_Should
    {
        [TestMethod]
        public async Task UpdateBrewery_Currectly()
        {
            var options = Utils.GetOptions(nameof(UpdateBrewery_Currectly));

            var providerMock = new Mock<IDateTimeProvider>();

            var brewery = new Brewery
            {
                Id = 3,
                Name = "Shumensko",
            };

            var breweryDTO = new BreweryDTO
            {
                Name = "Zagorka",
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Breweries.AddAsync(brewery);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BreweryService(assertContext, providerMock.Object);

                var update = await sut.Update(3, breweryDTO);

                var result = assertContext.Breweries.FirstOrDefault(dbBrewery => dbBrewery.Id == brewery.Id);


                //Assert
                Assert.IsTrue(result.Name == breweryDTO.Name);
            }
        }

        [TestMethod]
        public async Task ThrowExeption_OnWrongNameParams()
        {
            var options = Utils.GetOptions(nameof(UpdateBrewery_Currectly));

            var providerMock = new Mock<IDateTimeProvider>();

            var brewery = new Brewery
            {
                Id = 4,
                Name = "Shumensko",
            };

            var breweryDTO = new BreweryDTO
            {
                Name = "Z",
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Breweries.AddAsync(brewery);
                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BreweryService(actContext, providerMock.Object);

                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.Update(4, breweryDTO));
            }
        }
    }
}
