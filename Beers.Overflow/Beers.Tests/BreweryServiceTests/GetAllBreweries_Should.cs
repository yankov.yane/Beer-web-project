﻿using Beers.Data.Context;
using Beers.Models;
using Beers.Services;
using Beers.Services.Providers.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Beers.Tests.BeerServiceTests
{
    [TestClass]
    public class GetAllBreweries_Should
    {
        [TestMethod]
        public async Task ReturnAllBreweries_Correctly()
        {
            var options = Utils.GetOptions(nameof(ReturnAllBreweries_Correctly));

            var providerMock = new Mock<IDateTimeProvider>();

            var breweries = new List<Brewery>()
            {
                new Brewery
                {
                    Id = 1,
                    Name = "Kamenitza"
                },
                new Brewery
                {
                    Id = 2,
                    Name = "Zagorka"
                }
            };
            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.Breweries.AddRange(breweries);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BreweryService(assertContext, providerMock.Object);

                var result = await sut.GetAllBreweries();
                var breweryOne = result.FirstOrDefault(x => x.Name == breweries[0].Name);
                var breweryTwo = result.FirstOrDefault(x => x.Name == breweries[1].Name);

                //Assert
                Assert.IsNotNull(breweryOne);
                Assert.IsNotNull(breweryTwo);
            }
        }
    }
}
