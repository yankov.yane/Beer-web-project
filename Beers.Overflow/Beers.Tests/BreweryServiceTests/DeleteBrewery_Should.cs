﻿using Beers.Data.Context;
using Beers.Models;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Tests.BeerServiceTests
{
    [TestClass]
    public class DeleteBrewery_Should
    {
        [TestMethod]
        public async Task SetDeletedStatus_Currectly()
        {
            var options = Utils.GetOptions(nameof(SetDeletedStatus_Currectly));

            var providerMock = new Mock<IDateTimeProvider>();

            var brewery = new Brewery
            {
                Id = 1,
                Name = "Kamenitza",
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Breweries.AddAsync(brewery);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BreweryService(assertContext, providerMock.Object);

                var result = await sut.Delete(1);

                //Assert
                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task ThrowExeption_OnBreweryNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowExeption_OnBreweryNotFound));

            var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BreweryService(assertContext, providerMock.Object);

                var result = await sut.Delete(1);

                //Assert
                Assert.IsFalse(result);
            }
        }
    }
}
