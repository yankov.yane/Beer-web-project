﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Beers.Tests.StylesServiceTests
{
    [TestClass]
    public class GetStyleShould
    {
        [TestMethod]
        public async Task GetStyleCorrectly()
        {
            var options = Utils.GetOptions(nameof(GetStyleCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();

            var style = new StylesDTO()
            {
                StyleType = "Dark"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new StylesService(arrangeContext, providerMock.Object);
                await sut.Create(style);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new StylesService(assertContext, providerMock.Object);
                var result = await sut.GetStyle(1);

                Assert.AreEqual(result.StyleType, "Dark");
            }
        }


        [TestMethod]
        public async Task GetStyleThrowsWhenNotFound()
        {
            var options = Utils.GetOptions(nameof(GetStyleThrowsWhenNotFound));

            var providerMock = new Mock<IDateTimeProvider>();


            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new StylesService(assertContext, providerMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetStyle(1));
            }
        }
    }
}
