﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Beers.Tests.StylesServiceTests
{
    [TestClass]
    public class GetAllStylesShould
    {
        [TestMethod]
        public async Task GetAllStylesCorrectly()
        {
            var options = Utils.GetOptions(nameof(GetAllStylesCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();

            var style1 = new StylesDTO()
            {
                StyleType = "Dark"
            };
            var style2 = new StylesDTO()
            {
                StyleType = "Light"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new StylesService(arrangeContext, providerMock.Object);
                await sut.Create(style1);
                await sut.Create(style2);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new StylesService(assertContext, providerMock.Object);
                var result = await sut.GetAllStyles();

                Assert.AreEqual(result.Count(), 2);
            }
        }

        [TestMethod]
        public async Task GetAllStylesReturnsEmptyWhenNoDataOnDB()
        {
            var options = Utils.GetOptions(nameof(GetAllStylesReturnsEmptyWhenNoDataOnDB));

            var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new StylesService(assertContext, providerMock.Object);
                var result = await sut.GetAllStyles();

                Assert.AreEqual(result.Count(), 0);
            }
        }
    }
}
