﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Beers.Tests.StylesServiceTests
{
    [TestClass]
    public class UpdateShould
    {
        [TestMethod]
        public async Task UpdateStyleCorrectly()
        {
            var options = Utils.GetOptions(nameof(UpdateStyleCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();

            var oldStyle = new StylesDTO()
            {
                StyleType = "Dark"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new StylesService(arrangeContext, providerMock.Object);
                await sut.Create(oldStyle);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var newStyle = new StylesDTO()
                {
                    StyleType = "Light"
                };

                //Act
                var sut = new StylesService(assertContext, providerMock.Object);
                var result = await sut.Update(1, newStyle);

                Assert.AreEqual(newStyle.StyleType, result.StyleType);
                Assert.AreEqual(1, result.Id);
            }
        }

        [TestMethod]
        public async Task UpdateCountryThrowsWhenNewNameIsNull()
        {
            var options = Utils.GetOptions(nameof(UpdateCountryThrowsWhenNewNameIsNull));

            var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                StylesDTO newStyle = null;

                //Act
                var sut = new StylesService(assertContext, providerMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.Update(1, newStyle));
            }
        }

        [TestMethod]
        public async Task UpdateCountryThrowsWhenCountryNotFound()
        {
            var options = Utils.GetOptions(nameof(UpdateCountryThrowsWhenCountryNotFound));

            var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                var newStyle = new StylesDTO()
                {
                    StyleType = "Light"
                };

                //Act
                var sut = new StylesService(assertContext, providerMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.Update(100, newStyle));
            }
        }
    }
}
