﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Beers.Tests.StylesServiceTests
{
    [TestClass]
    public class CreateShould
    {
        [TestMethod]
        public async Task CreateStyleCorrectly()
        {
            var options = Utils.GetOptions(nameof(CreateStyleCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();

            var style = new StylesDTO()
            {
                StyleType = "Dark"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new StylesService(arrangeContext, providerMock.Object);
                await sut.Create(style);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                string expected = "Dark";

                //Act
                var sut = new StylesService(assertContext, providerMock.Object);
                var result = await sut.GetStyle(1);

                Assert.AreEqual(expected, result.StyleType);
                Assert.AreEqual(1, result.Id);
            }
        }

        [TestMethod]
        public async Task CreateStyleThrowsWhenStyleNull()
        {
            var options = Utils.GetOptions(nameof(CreateStyleThrowsWhenStyleNull));

            var providerMock = new Mock<IDateTimeProvider>();

            StylesDTO style = null;

            using (var assertContext = new BeerOverflowContext(options))
            {
                var sut = new StylesService(assertContext, providerMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.Create(style));
            }
        }
    }
}
