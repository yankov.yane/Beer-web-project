﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Beers.Tests.CountryServiceTests
{
    [TestClass]
    public class GetAllCountries
    {
        [TestMethod]
        public async Task GetAllCountriesCorrectly()
        {
            var options = Utils.GetOptions(nameof(GetAllCountriesCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();

            var country = new CountryDTO()
            {
                Name = "Bulgaria"
            };

            var country2 = new CountryDTO()
            {
                Name = "Spain"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(arrangeContext, providerMock.Object);
                await sut.Create(country);
                await sut.Create(country2);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new CountryService(assertContext, providerMock.Object);
                var result = await sut.GetAllCountries();

                Assert.AreEqual(result.Count(), 2);
            }
        }

        [TestMethod]
        public async Task GetAllCountriesReturnsEmptyWhenNoDataOnDB()
        {
            var options = Utils.GetOptions(nameof(GetAllCountriesReturnsEmptyWhenNoDataOnDB));

            var providerMock = new Mock<IDateTimeProvider>();


            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new CountryService(assertContext, providerMock.Object);
                var result = await sut.GetAllCountries();

                Assert.AreEqual(result.Count(), 0);
            }
        }
    }
}
