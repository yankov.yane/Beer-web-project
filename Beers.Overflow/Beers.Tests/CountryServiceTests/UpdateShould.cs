﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Beers.Tests.CountryServiceTests
{
    [TestClass]
    public class UpdateShould
    {
        [TestMethod]
        public async Task UpdateCountryCorrectly()
        {
            var options = Utils.GetOptions(nameof(UpdateCountryCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();

            var country = new CountryDTO()
            {
                Name = "Bulgaria"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(arrangeContext, providerMock.Object);
                await sut.Create(country);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                var newCountry = new CountryDTO()
                {
                    Name = "Spain"
                };
                
                //Act
                var sut = new CountryService(assertContext, providerMock.Object);
                var result = await sut.Update(1, newCountry);

                Assert.AreEqual(newCountry.Name, result.Name);
                Assert.AreEqual(1, result.Id);
            }
        }

        [TestMethod]
        public async Task UpdateCountryThrowsWhenNewNameIsNull()
        {
            var options = Utils.GetOptions(nameof(UpdateCountryThrowsWhenNewNameIsNull));

            var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {

                CountryDTO newCountry = null;
                //Act
                var sut = new CountryService(assertContext, providerMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.Update(1, newCountry));
            }
        }

        [TestMethod]
        public async Task UpdateCountryThrowsWhenCountryNotFound()
        {
            var options = Utils.GetOptions(nameof(UpdateCountryThrowsWhenCountryNotFound));

            var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                var newCountry = new CountryDTO()
                {
                    Name = "Spain"
                };

                //Act
                var sut = new CountryService(assertContext, providerMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.Update(100, newCountry));
            }
        }
    }
}
