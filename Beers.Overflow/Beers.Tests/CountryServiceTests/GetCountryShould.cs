﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Beers.Tests.CountryServiceTests
{
    [TestClass]
    public class GetStyleShould
    {
        [TestMethod]
        public async Task GetCountryCorrectly()
        {
            var options = Utils.GetOptions(nameof(GetCountryCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();

            var country = new CountryDTO()
            {
                Name = "Bulgaria"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(arrangeContext, providerMock.Object);
                await sut.Create(country);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new CountryService(assertContext, providerMock.Object);
                var result = await sut.GetCountry(1);

                Assert.AreEqual(result.Name, "Bulgaria");
            }
        }


        [TestMethod]
        public async Task GetCountryThrowsWhenNotFound()
        {
            var options = Utils.GetOptions(nameof(GetCountryThrowsWhenNotFound));

            var providerMock = new Mock<IDateTimeProvider>();


            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new CountryService(assertContext, providerMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetCountry(1));
            }
        }
    }
}
