﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Beers.Tests.CountryServiceTests
{
    [TestClass]
    public class DeleteShould
    {
        [TestMethod]
        public async Task DeleteCountryCorrectly()
        {
            var options = Utils.GetOptions(nameof(DeleteCountryCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();

            var country = new CountryDTO()
            {
                Name = "Bulgaria"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(arrangeContext, providerMock.Object);
                await sut.Create(country);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new CountryService(assertContext, providerMock.Object);
                var result = await sut.Delete(1);

                Assert.AreEqual(result, true);
            }
        }

        [TestMethod]
        public async Task DeleteCountryThrowsWhenCountryNotFound()
        {
            var options = Utils.GetOptions(nameof(DeleteCountryThrowsWhenCountryNotFound));

            var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new CountryService(assertContext, providerMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.Delete(2));
            }
        }
    }
}
