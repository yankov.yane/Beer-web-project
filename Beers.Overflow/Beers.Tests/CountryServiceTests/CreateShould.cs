﻿using Beers.Data.Context;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Beers.Tests.CountryServiceTests
{
    [TestClass]
    public class CreateShould
    {
        [TestMethod]
        public async Task CreateCountryCorrectly()
        {
            var options = Utils.GetOptions(nameof(CreateCountryCorrectly));

            var providerMock = new Mock<IDateTimeProvider>();

            var country = new CountryDTO()
            {
                Name = "Bulgaria"
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new CountryService(arrangeContext, providerMock.Object);
                await sut.Create(country);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                string expected = "Bulgaria";

                //Act
                var sut = new CountryService(assertContext, providerMock.Object);
                var result = await sut.GetCountry(1);

                Assert.AreEqual(expected, result.Name);
                Assert.AreEqual(1, result.Id);
            }
        }

         
        [TestMethod]
        public async Task CreateCountryThrowsWhenInvalidId()
        {
            var options = Utils.GetOptions(nameof(CreateCountryThrowsWhenInvalidId));

            var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new CountryService(assertContext, providerMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetCountry(1));
            }
        }
    }
}
