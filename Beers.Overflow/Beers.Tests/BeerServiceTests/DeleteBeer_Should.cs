﻿using Beers.Data.Context;
using Beers.Models;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Tests.BeerServiceTests
{
    [TestClass]
    public class DeleteBeer_Should
    {
        [TestMethod]
        public async Task DeleteBeer_Correctly()
        {
            var options = Utils.GetOptions(nameof(DeleteBeer_Correctly));

            var providerMock = new Mock<IDateTimeProvider>();

            var beer = new Beer
            {
                Id = 5,
                Name = "Kamenitza",
                Description = "This is a test description",
                ABV = 4.5m,
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Beers.AddAsync(beer);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerService(assertContext, providerMock.Object);

                var result = await sut.Delete(5);

                //Assert
                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public async Task ThrowExeption_OnBeerNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowExeption_OnBeerNotFound));

            var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerService(assertContext, providerMock.Object);

                var result = await sut.Delete(1);

                //Assert
                Assert.IsFalse(result);
            }
        }
    }
}
