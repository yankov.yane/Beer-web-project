﻿using Beers.Data.Context;
using Beers.Models;
using Beers.Services;
using Beers.Services.Providers.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Threading.Tasks;

namespace Beers.Tests.BeerServiceTests
{
    [TestClass]
    public class GetBeer_Should
    {
        [TestMethod]
        public async Task ReturnBeer_Correctly()
        {
            var options = Utils.GetOptions(nameof(ReturnBeer_Correctly));

            var providerMock = new Mock<IDateTimeProvider>();

            var beer = new Beer()
            {
                Name = "American Amber Ale",
                Description = "Like most amber beers, American amber ale is named after the golden to amber color this American version...",
                BreweryId = 1,
                Brewery = new Brewery(),
                ABV = 4.4M,
                StyleId = 1,
                Style = new Style(),
                Brand = "Bud-Light",
                CreatedOn = DateTime.Now
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Beers.AddAsync(beer);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerService(assertContext, providerMock.Object);

                var result = await sut.GetBeer(1);

                //Assert
                Assert.IsTrue(result.Name == beer.Name);
                Assert.IsTrue(result.Description == beer.Description);
                Assert.IsTrue(result.ABV == beer.ABV);
            }
        }

        [TestMethod]
        public async Task ThrowExeption_OnBeerNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowExeption_OnBeerNotFound));

            var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerService(assertContext, providerMock.Object);

                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetBeer(1));
            }
        }
    }
}
