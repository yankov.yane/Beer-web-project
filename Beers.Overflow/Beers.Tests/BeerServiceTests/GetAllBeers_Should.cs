﻿using Beers.Data.Context;
using Beers.Models;
using Beers.Services;
using Beers.Services.Providers.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Beers.Tests.BeerServiceTests
{
    [TestClass]
    public class GetAllBeers_Should
    {
        [TestMethod]
        public async Task ReturnAllBeers_Correctly()
        {
            var options = Utils.GetOptions(nameof(ReturnAllBeers_Correctly));

            var providerMock = new Mock<IDateTimeProvider>();

            var beer1 = new Beer()
            {
                Name = "American Amber Ale",
                Description = "Like most amber beers, American amber ale is named after the golden to amber color this American version...",
                BreweryId = 1,
                Brewery = new Brewery(),
                ABV = 4.4M,
                StyleId = 1,
                Style = new Style(),
                Brand = "Bud-Light",
                CreatedOn = DateTime.Now
            };
            var beer2 = new Beer()
            {
                Name = "American Amber Lager",
                Description = "A widely available, sessionable craft beer style that showcases both malt and hops. Amber lagers are...",
                BreweryId = 1,
                Brewery = new Brewery(),
                ABV = 4.8M,
                StyleId = 2,
                Style = new Style(),
                Brand = "Bud-Light",
                CreatedOn = DateTime.Now

            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Beers.AddAsync(beer1);
                await arrangeContext.Beers.AddAsync(beer2);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerService(assertContext, providerMock.Object);

                var result = await sut.GetAllBeers();
                var beerOne = result.FirstOrDefault(x => x.Name == beer1.Name);
                var beerTwo = result.FirstOrDefault(x => x.Name == beer2.Name);

                //Assert
                Assert.IsNotNull(beerOne);
                Assert.IsNotNull(beerTwo);
            }
        }
    }
}
