﻿using Beers.Data.Context;
using Beers.Models;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beers.Tests.BeerServiceTests
{
    [TestClass]
    public class UpdateBeer_Should
    {
        [TestMethod]
        public async Task UpdateBeer_Correctly()
        {
            var options = Utils.GetOptions(nameof(UpdateBeer_Correctly));

            var providerMock = new Mock<IDateTimeProvider>();

            var beer = new Beer
            {
                Id = 1,
                Name = "Kamenitza",
                Description = "This is a test description",
                ABV = 4.5m,
            };

            var beerDTO = new BeerDTO
            {
                Name = "Zagorka",
                Description = "This is another test description",
                ABV = 4.8m,
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                await arrangeContext.Beers.AddAsync(beer);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerService(assertContext, providerMock.Object);

                var update = await sut.Update(1, beerDTO);

                var result = assertContext.Beers.FirstOrDefault(x => x.Id == beer.Id);


                //Assert
                Assert.IsTrue(result.Name == beerDTO.Name);
                Assert.IsTrue(result.Description == beerDTO.Description);
                Assert.IsTrue(result.ABV == beerDTO.ABV);
            }
        }

        [TestMethod]
        public async Task ThrowExeption_OnIncorrectNameParams()
        {
            var options = Utils.GetOptions(nameof(ThrowExeption_OnIncorrectNameParams));

            var providerMock = new Mock<IDateTimeProvider>();

            var beer = new Beer
            {
                Id = 2,
                Name = "Kamenitza",
                Description = "This is a test description",
                ABV = 4.5m,
            };

            var beerDTO = new BeerDTO
            {
                Name = "Z",
                Description = "This is a test description",
                ABV = 4.8m,
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.Beers.Add(beer);
                arrangeContext.SaveChanges();
            }

            using (var actContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerService(actContext, providerMock.Object);

                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.Update(1,beerDTO));
            }
        }

        [TestMethod]
        public async Task ThrowExeption_OnIncorrectDecriptionParams()
        {
            var options = Utils.GetOptions(nameof(ThrowExeption_OnIncorrectDecriptionParams));

            var providerMock = new Mock<IDateTimeProvider>();

            var beer = new Beer
            {
                Id = 1,
                Name = "Kamenitza",
                Description = "This is a test description",
                ABV = 4.5m,
            };

            var beerDTO = new BeerDTO
            {
                Name = "Zagorka",
                Description = "Test",
                ABV = 4.8m,
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                arrangeContext.Beers.Add(beer);
                arrangeContext.SaveChanges();
            }

            using (var actContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerService(actContext, providerMock.Object);

                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.Update(1, beerDTO));
            }
        }
    }
}
