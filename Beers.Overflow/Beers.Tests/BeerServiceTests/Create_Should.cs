﻿using Beers.Data.Context;
using Beers.Models;
using Beers.Services;
using Beers.Services.DTOs;
using Beers.Services.Providers.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Beers.Tests.BeerServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public void CreateBeer_Currectly()
        {
            var options = Utils.GetOptions(nameof(CreateBeer_Currectly));

            var providerMock = new Mock<IDateTimeProvider>();
            var beerDTO = new BeerDTO
            {
                Id = 1,
                Name = "Kamenitza",
                Description = "This is a test description",
                ABV = 4.5m,
            };

            using (var arrangeContext = new BeerOverflowContext(options))
            {
                var sut = new BeerService(arrangeContext, providerMock.Object);
                var beer = sut.Create(beerDTO);
            }

            using (var assertContext = new BeerOverflowContext(options))
            {
                //Act
                var result = assertContext.Beers.FirstOrDefault(x=> x.Id == beerDTO.Id);
                
                //Assert
                Assert.IsTrue(result.Name == beerDTO.Name);
                Assert.IsTrue(result.Description == beerDTO.Description);
                Assert.IsTrue(result.ABV == beerDTO.ABV);
            }
        }

        [TestMethod]
        public async Task ThrowExeptionOn_IncorrectName()
        {
            var options = Utils.GetOptions(nameof(ThrowExeptionOn_IncorrectName));

            var providerMock = new Mock<IDateTimeProvider>();

            var beerDTO = new BeerDTO
            {
                Id = 1,
                Name = "Q",
                Description = "This is a test description",
                ABV = 4.5m,
            };

            using (var actContext = new BeerOverflowContext(options))
            {
                //Act
                var sut = new BeerService(actContext, providerMock.Object);

                //Assert
                await Assert.ThrowsExceptionAsync<ArgumentOutOfRangeException>(() => sut.Create(beerDTO));
            }
        }
    }
}
